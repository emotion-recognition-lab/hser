# Human Multimodal Emotion Recognition Laboratory (HESR Lab)
### Seyed Sadegh Hosseini, Mohammad Reza Yamaghani, Soudabeh Arabani Pourzaker
sadeghhosseini@malayeru.ac.ir , O_yamaghani@liau.ac.ir , soodabeharabani@gmail.com

---
### Introduction
The HESR lab software receives audio, text, image, video, and online data and extracts emotions by applying artificial intelligence, machine learning, and audio, text, and video (A/T/V) processing. For example, a movie separates into frames and audio (V/A), and the text is extracted from the audio file when uploaded or information is entered into the system online. Emotions extracts using the models designed and implemented based on A/T/V. In order to acquire higher accuracy, the emotional states are extracted from the common features of A/T, A/V, and T/V. Finally, more complicated emotions are extracted and presented to the user in the form of a final report utilizing the common features of A/V/T in each time period. As shown in Fig. 3, related charts and tables are provided to the user for a better understanding so that the feature of each moment can be observed in the visual data while speaking. 
In addition, the ability to archive based on people, time, date, analyzer, and the like are predicted in the above-mentioned software, which facilitates access to the history of emotion recognition for individuals.


### Live Demo:

Demo: [Link to Video](https://emotion.shosseini.com/upload/EML%201%20-%20t%20720.mp4 "Link to Video") 

URL: https://emotion.shosseini.com/

Username: demo

Password: demo

### Installation

The HESR software can work on all Linux-based OS with minimum hardware requirements. It is possible to use HESR on SBC like Raspberry Pi. To have a smooth experience and fast results, we suggest running the software on a machine with the following configurations:
 - OS: Debian, Ubuntu
 - Minimum RAM: 8 GB
 - CPU Core: 4
 - Hard Storage: 120 GB SSD
 - GPU


The HESR has two parts, User Interface ( Web APP) and ML Server.

**ML Server** is a Python Service that provides APIs for the WebApp project and handles all work around extracting emotions from different data sources.

**WebApp** is a NodeJs application that provides a web-based user interface to receive the data (Video, Audio, and Text) from the user and researcher, prepare the data, and communicate with Python Service. It also stores all historical data and makes a searchable dictionary.

It is necessary to run two projects on the same machine.

To set up the Python Service, follow this instruction:  [Link](/src/ml-server/README.md "Link")

To set up the WebApp, follow this instruction:  [Link](/src/web-app/README.md "Link")


### Licence

GNU AGPLv3

Permissions of this strongest copyleft license are conditioned on making available complete source code of licensed works and modifications, which include larger works using a licensed work, under the same license. Copyright and license notices must be preserved. Contributors provide an express grant of patent rights. When a modified version is used to provide a service over a network, the complete source code of the modified version must be made available.