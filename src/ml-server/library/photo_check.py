

import numpy as np

import cv2
import dlib
from imutils import face_utils
from scipy.spatial import distance
from scipy.ndimage import zoom


### Model ###
from tensorflow.keras.models import load_model

# Load the pre-trained X-Ception vosk
model = load_model('Models/video.h5')


# Load the facial landmarks predictor
predictor_landmarks = dlib.shape_predictor("Models/face_landmarks.dat")

# Load the face detector
face_detect = dlib.get_frontal_face_detector()

def photoEmotionRecognition(filePath):
    # Image shape
    shape_x = 48
    shape_y = 48

    print("PHOTO_EMOTION_RECOGNITION:: start")
    print("PHOTO_EMOTION_RECOGNITION:: read image: " + filePath)

    # Emotion mapping
    emotion = {0: 'Angry', 1: 'Disgust', 2: 'Fear', 3: 'Happy', 4: 'Neutral', 5: 'Sad', 6: 'Surprise'}

    frame = cv2.imread(filePath)

    # Initiate Landmarks
    (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
    (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    (nStart, nEnd) = face_utils.FACIAL_LANDMARKS_IDXS["nose"]
    (mStart, mEnd) = face_utils.FACIAL_LANDMARKS_IDXS["mouth"]
    (jStart, jEnd) = face_utils.FACIAL_LANDMARKS_IDXS["jaw"]

    (eblStart, eblEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eyebrow"]
    (ebrStart, ebrEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eyebrow"]


    # Prediction vector
    predictions = []

    # Emotion vectors
    angry_0 = []
    disgust_1 = []
    fear_2 = []
    happy_3 = []
    sad_4 = []
    surprise_5 = []
    neutral_6 = []

    # Image to gray scale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # All faces detected
    rects = face_detect(gray, 1)

    # Count number of eye blinks (not used in vosk prediction)
    def eye_aspect_ratio(eye):

        A = distance.euclidean(eye[1], eye[5])
        B = distance.euclidean(eye[2], eye[4])
        C = distance.euclidean(eye[0], eye[3])
        ear = (A + B) / (2.0 * C)

        return ear

    # For each detected face
    for (i, rect) in enumerate(rects):

        # Identify face coordinates
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        face = gray[y:y + h, x:x + w]

        # Identify landmarks and cast to numpy
        shape = predictor_landmarks(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # Zoom on extracted face
        face = zoom(face, (shape_x / face.shape[0], shape_y / face.shape[1]))

        # Cast type float
        face = face.astype(np.float32)

        # Scale the face
        face /= float(face.max())
        face = np.reshape(face.flatten(), (1, 48, 48, 1))

        # Make Emotion prediction on the face, outputs probabilities
        prediction = model.predict(face)

        # For plotting purposes with Altair
        angry_0.append(prediction[0][0].astype(float))
        disgust_1.append(prediction[0][1].astype(float))
        fear_2.append(prediction[0][2].astype(float))
        happy_3.append(prediction[0][3].astype(float))
        sad_4.append(prediction[0][4].astype(float))
        surprise_5.append(prediction[0][5].astype(float))
        neutral_6.append(prediction[0][6].astype(float))

        # Most likely emotion
        prediction_result = np.argmax(prediction)

        # Append the emotion to the final list
        predictions.append(str(prediction_result))

        # Draw rectangle around the face
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Top left : Put the ID of the face
        cv2.putText(frame, "Face #{}".format(i + 1), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # Draw all the landmarks dots
        for (j, k) in shape:
            cv2.circle(frame, (j, k), 1, (0, 0, 255), -1)

        # Add prediction probabilities on the top-left report
        cv2.putText(frame, "----------------", (40, 100 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 155, 0)
        cv2.putText(frame, "Emotional report : Face #" + str(i + 1), (40, 120 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    155, 0)
        cv2.putText(frame, "Angry : " + str(round(prediction[0][0], 3)), (40, 140 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, 155, 0)
        cv2.putText(frame, "Disgust : " + str(round(prediction[0][1], 3)), (40, 160 + 180 * i),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, 155, 0)
        cv2.putText(frame, "Fear : " + str(round(prediction[0][2], 3)), (40, 180 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, 155, 1)
        cv2.putText(frame, "Happy : " + str(round(prediction[0][3], 3)), (40, 200 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, 155, 1)
        cv2.putText(frame, "Sad : " + str(round(prediction[0][4], 3)), (40, 220 + 180 * i), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, 155, 1)
        cv2.putText(frame, "Surprise : " + str(round(prediction[0][5], 3)), (40, 240 + 180 * i),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, 155, 1)
        cv2.putText(frame, "Neutral : " + str(round(prediction[0][6], 3)), (40, 260 + 180 * i),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, 155, 1)

        # Annotate main image with the emotion label
        if prediction_result == 0:
            cv2.putText(frame, "Angry", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif prediction_result == 1:
            cv2.putText(frame, "Disgust", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif prediction_result == 2:
            cv2.putText(frame, "Fear", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif prediction_result == 3:
            cv2.putText(frame, "Happy", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif prediction_result == 4:
            cv2.putText(frame, "Sad", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif prediction_result == 5:
            cv2.putText(frame, "Surprise", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        else:
            cv2.putText(frame, "Neutral", (x + w - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

        # Eye Detection and Blink Count
        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]

        # Compute Eye Aspect Ratio
        leftEAR = eye_aspect_ratio(leftEye)
        rightEAR = eye_aspect_ratio(rightEye)
        ear = (leftEAR + rightEAR) / 2.0

        # And plot its contours
        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)
        cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

        # Detect Nose and draw its contours
        nose = shape[nStart:nEnd]
        noseHull = cv2.convexHull(nose)
        cv2.drawContours(frame, [noseHull], -1, (0, 255, 0), 1)

        # Detect Mouth and draw its contours
        mouth = shape[mStart:mEnd]
        mouthHull = cv2.convexHull(mouth)
        cv2.drawContours(frame, [mouthHull], -1, (0, 255, 0), 1)

        # Detect Jaw and draw its contours
        jaw = shape[jStart:jEnd]
        jawHull = cv2.convexHull(jaw)
        cv2.drawContours(frame, [jawHull], -1, (0, 255, 0), 1)

        # Detect Eyebrows and draw its contours
        ebr = shape[ebrStart:ebrEnd]
        ebrHull = cv2.convexHull(ebr)
        cv2.drawContours(frame, [ebrHull], -1, (0, 255, 0), 1)

        ebl = shape[eblStart:eblEnd]
        eblHull = cv2.convexHull(ebl)
        cv2.drawContours(frame, [eblHull], -1, (0, 255, 0), 1)

    # Show number of faces captured
    cv2.putText(frame, 'Number of Faces : ' + str(len(rects)), (40, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, 155, 1)
    print('Number of Faces : ' + str(len(rects)), (40, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, 155, 1)

    rows = [angry_0, disgust_1, fear_2, happy_3, sad_4, surprise_5, neutral_6]

    resultPath = filePath.replace('.jpg', '-info.jpg')
    cv2.imwrite(resultPath, frame)
    result = dict()
    result['path'] = resultPath
    result['mapping'] = emotion
    result['rows'] = rows
    result['prediction'] = predictions
    return result
