#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer
import json
import os

if not os.path.exists("Models/vosk"):
    print ("Please download the vosk from https://alphacephei.com/vosk/models and unpack as 'vosk' in the current folder.")
    exit (1)


model = Model("Models/vosk")

# Large vocabulary free form recognition
rec = KaldiRecognizer(model, 16000)

# You can also specify the possible word list
#rec = KaldiRecognizer(vosk, 16000, "zero oh one two three four five six seven eight nine")

def speechToText(file):
    wf = open(file, "rb")
    wf.read(44) # skip header

    output = ''
    while True:
        data = wf.read()
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            res = json.loads(rec.Result())
            # output = output + res['text']
            output = res

    return output
