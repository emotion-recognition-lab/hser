from library.speech_emotion_recognition import *
import numpy
from json import JSONEncoder

model_sub_dir = os.path.join('Models', 'audio.hdf5')

SER = speechEmotionRecognition(model_sub_dir)


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

def voiceEmotionRecognition(filePath):
    rec_sub_dir = os.path.join(filePath)
    # Predict emotion in voice at each time step
    step = 1  # in sec
    sample_rate = 16000  # in kHz
    emotions, timestamp, emotion, X = SER.predict_emotion_from_file(rec_sub_dir, chunk_step=step * sample_rate, sample_rate=sample_rate)

    emotion_dist = [int(100 * emotions.count(emotion) / len(emotions)) for emotion in SER._emotion.values()]
    result = dict()
    result['mapping'] = emotion
    result['prediction'] = emotion_dist
    result['row'] = emotions
    return result
