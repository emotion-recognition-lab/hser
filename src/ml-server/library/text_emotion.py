#!/usr/bin/python

from transformers import TFBertModel, BertTokenizerFast, BertConfig
from tensorflow.keras.layers import Input, Dropout, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.initializers import TruncatedNormal
from library.cleanup import preprocess_corpus, tokenize

# Setting the taxonomy (including 'Neutral')
GE_taxonomy = ["admiration", "amusement", "anger", "annoyance", "approval", "caring",
                "confusion", "curiosity", "desire", "disappointment", "disapproval",
                "disgust", "embarrassment", "excitement", "fear", "gratitude", "grief",
                "joy", "love", "nervousness", "optimism", "pride", "realization",
                "relief", "remorse", "sadness", "surprise", "neutral"]


# Loading BERT base model configuration
config = BertConfig.from_pretrained('bert-base-uncased', output_hidden_states=False)

# Loading BERT tokenizer
tokenizer = BertTokenizerFast.from_pretrained(pretrained_model_name_or_path='bert-base-uncased', config=config)

def predict_sample(text_sample, model, tokenizer):
    # Clean text
    text_sample = preprocess_corpus(text_sample)

    # Tokenize text
    sample = tokenize(tokenizer, text_sample)

    # Probability predictions
    sample_probas = model.predict(sample)
    sample_probas = sample_probas.ravel().tolist()

    print(sample_probas)
    print(GE_taxonomy)

    result = dict()
    result['emotions'] = GE_taxonomy
    result['predictions'] = sample_probas
    return result

def create_model(nb_labels, max_length=48, model_name='bert-base-uncased'):

    config = BertConfig.from_pretrained('bert-base-uncased', output_hidden_states=False)

    # Loading BERT main layer
    transformer_model = TFBertModel.from_pretrained(model_name, config = config)
    bert = transformer_model.layers[0]

    # Build the model inputs
    input_ids = Input(shape=(max_length,), name='input_ids', dtype='int32')
    attention_mask = Input(shape=(max_length,), name='attention_mask', dtype='int32')
    token_ids = Input(shape=(max_length,), name='token_ids', dtype='int32')
    inputs = {'input_ids': input_ids, 'attention_mask': attention_mask, 'token_ids': token_ids}

    # Load the Transformers BERT model as a layer in a Keras model
    bert_model = bert(inputs)[1]
    dropout = Dropout(config.hidden_dropout_prob, name='pooled_output')
    pooled_output = dropout(bert_model, training=False)

    # Then build your model output
    emotion = Dense(units=nb_labels, activation="sigmoid", kernel_initializer=TruncatedNormal(stddev=config.initializer_range), name='emotion')(pooled_output)
    outputs = emotion

    # And combine it all in a model object
    model = Model(inputs=inputs, outputs=outputs, name='BERT_MultiLabel')

   # return model
    return model

def load_all(model, weights):
    model.load_weights(weights)
    return model

def getTextPred(text):
    result = predict_sample(text, model, tokenizer)
    return result

model = create_model(27)
model = load_all(model, "./Models/bert-weights.hdf5")
