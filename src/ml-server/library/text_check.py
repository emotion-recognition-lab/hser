### Text imports ###
from library.text_emotion_recognition import *
import pandas as pd
from library.text_emotion import getTextPred
import os
import json

def textEmotionRecognition(filePath):
    rec_sub_dir = os.path.join(filePath)
    text = open(rec_sub_dir, "r").read()
    text = json.loads(text)
    print(text)
    text = text["text"]
    def get_personality(text):
        try:
            pred = predict().run(text, model_name="Personality_traits_NN")
            return pred
        except KeyError:
            return None

    # Predict emotion in voice at each time step
    traits = ['Extraversion', 'Neuroticism', 'Agreeableness', 'Conscientiousness', 'Openness']
    probas = get_personality(text)[0].tolist()

    perso = {}
    perso['Extraversion'] = probas[0]
    perso['Neuroticism'] = probas[1]
    perso['Agreeableness'] = probas[2]
    perso['Conscientiousness'] = probas[3]
    perso['Openness'] = probas[4]


    df_text_perso = pd.DataFrame.from_dict(perso, orient='index')
    df_text_perso = df_text_perso.reset_index()
    df_text_perso.columns = ['Trait', 'Value']



    result = dict()
    result['path'] = filePath
    result['mapping'] = traits
    result['prediction'] = perso
    result['emotions'] = getTextPred(text)

    return result
