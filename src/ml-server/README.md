
# ML-Server
## ML Service web server

### Description
ML-Server is the application that runs the ML machine and exposes APIs for the Web Application.

### Requirements:
Linux / macOS
Pyhton 3.8

### Installation:
You only need to install the dependencies:
```sh
pip3 install -r requirements.txt
```

*note: based on the operation system and your environment, it is possible that you need to install more packages; in this case, you need to follow the necessary package installation constructions.*


### Configuration:
On the ML-Server, you must define the upload directory (inside the web app project). Open the `server.py` and fill the the `uploadPath` variable by the full path of the upload directory.

### Run The Server:
You can run the server by running the `service.py` file.
```sh
python3.8 server.py
```
This command runs the ML-Server on `http://localhost:8088`. Make sure this address is 
exist in `.env` file on Wep-App project for `SERVICE_URL`.
