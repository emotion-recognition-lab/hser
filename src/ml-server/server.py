#!/usr/bin/python
import sys
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse as urlparse
from urllib.parse import parse_qs



original_stdout = sys.stdout # Save a reference to the original standard output

with open('filename.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print('This message will be written to a file.')
    sys.stdout = original_stdout # Reset the standard output to its original value



# from library.photo_check import photoEmotionRecognition
# from library.voice_check import voiceEmotionRecognition
# from library.speech_to_text import speechToText
from library.text_check import textEmotionRecognition


hostName = "172.17.0.1"
serverPort = 8088
uploadPath = '/root/eml/'

class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("logfile.log", "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
            #this flush method is needed for python 3 compatibility.
            #this handles the flush command by doing nothing.
            #you might want to specify some extra behavior here.    
        pass    

sys.stdout = Logger()

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):


        try:
            parsed = urlparse.urlparse(self.path)
            if parsed.path == '/photo':
                print(self.path)
                filePath = uploadPath + parse_qs(parsed.query)["path"][0]
                result = photoEmotionRecognition(filePath)
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes(json.dumps(result, indent=4), "utf-8"))

            elif parsed.path == '/voice':
                print(self.path)
                filePath = uploadPath + parse_qs(parsed.query)["path"][0]
                result = voiceEmotionRecognition(filePath)
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                print(result)
                self.wfile.write(bytes(json.dumps(result, indent=4), "utf-8"))

            elif parsed.path == '/text':
                filePath = uploadPath + parse_qs(parsed.query)["path"][0]
                result = textEmotionRecognition(filePath)
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes(json.dumps(result, indent=4), "utf-8"))

            elif parsed.path == '/live':
                result = dict()
                result['live'] = True
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes(json.dumps(result, indent=4), "utf-8"))

            elif parsed.path == '/voice-to-speech':
                print(self.path)
                filePath = uploadPath + parse_qs(parsed.query)["path"][0]
                result = speechToText(filePath)
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes(json.dumps(result, indent=4), "utf-8"))

            elif parsed.path == '/update':
                print(self.path)
                import nltk
                dler = nltk.downloader.Downloader()
                dler._update_index()
                dler._status_cache['panlex_lite'] = 'installed'  # Trick the index to treat panlex_lite as it's already installed.
                dler.download('all')
                self.send_response(200)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes('updated', "utf-8"))

            else:
                self.send_response(404)
                self.send_header("Content-type", "application/json")
                self.end_headers()
                self.wfile.write(bytes('Not Found', "utf-8"))

        except ValueError:
            self.send_response(550)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            print(ValueError)
            self.wfile.write(bytes(ValueError, "utf-8"))

if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
