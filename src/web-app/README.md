# Emotion Recognation Lab - Web Application
## End User and Researcher Interface

### Description:
Web Application is a web-based software that handles all user interfaces and dashboards to enable users to work with Service features.

This project is running a web server on your machine or server. Then you can have access to the application through your browser.

*note:*  You should run this application on the same machine you ran, the *python-service*, as they use the same resources, files, and shared data.

------
### Preinstallation:
1- Installing `Nodejs`:
Follow the installation construction oth the `NodeJs` web site:
https://nodejs.org/en/download/package-manager/

2- Installing `yarn`:
```sh
npm install --global yarn
```

3- Installing `ffmpeg`:
Follow the main construction on the `ffmpeg` document. https://ffmpeg.org/

### Installation:
1- Set NodJs environment variable:
```sh
export NODE_OPTIONS="--openssl-legacy-provider" 
```

2- Install dependencies:
```sh
yarn install
```

3- Build the project
```sh
yarn build
```

### Running the Project:
Run the project by the bellow command:
```sh
yarn start
```

### Open The Project
Now you can see the web application project on your browser:
http://localhost:3000

Please use `chorme` as your web browser to have access to all features.

----

*Note: you do not need to go through installation whenever you want to run the project. You only need to do that when you update the project.*


