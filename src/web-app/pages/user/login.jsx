import {Form, Input, Button, Checkbox, Card, Layout, Space, message, Alert} from 'antd';
import React, {useEffect} from "react";
import Title from "antd/lib/typography/Title";
import ReactCanvasNest from 'react-canvas-nest';
import axios from "axios";
import './../../styles/global.less';
import {useRouter} from "next/router";
import {useUser} from "../../lib/useUser";

const tailLayout = {
    wrapperCol: {offset: 8, span: 16},
};

const Login = () => {
    const router = useRouter()
    // const [ user, {mutate}] = useUser()
    //
    // // useEffect(() => {
    // //     // Prefetch the dashboard page
    // //
    // //     // axios.post('/api/user/users', {username: 'ehsan', password: 'ehsan', remember: true})
    // //
    // //     router.prefetch('/')
    // // }, [])

    // const { mutateUser } = useUser();

    const onFinish = (values) => {
        message.destroy();
        axios.post('/api/user/login', values)
            .then((resp) => {
                if (JSON.stringify(resp.data)) {
                    localStorage.setItem('user', JSON.stringify(resp.data));
                    setTimeout(() => {
                        router.push('/');

                    }, 1000)
                }
            }).catch((e) => {
            console.log(e)
            message.error('Username or Password are not correct!')
        });

    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Layout style={{minHeight: '100vh'}}>

            <ReactCanvasNest className = 'canvasNest' count={150} config = {{ lineColor: '150, 150 ,150', pointColor: ' 224, 224, 224 ', follow: true }} style = {{ zIndex: 0 }} />
            <Alert
                message="Warning"
                description={`Because of the internet connection problem, you may face some issues in the application functionality.\n Please be patient and try again later.`}
                type="warning"
                closable={true}
                banner={true}
            />
            <Space align={'center'} direction={"vertical"} style={{height: '100vh', justifyContent: 'center', zIndex: 20}}>

                    <Space align={'center'} direction={"vertical"} >
                        <img src={'/icon.svg'} className={'brand-logo'} width={140} alt={'Human Emotion & Sensitivity Laboratory'}/>
                        <Title level={3}>Human Emotion & Sensitivity Laboratory</Title>
                    </Space>
                    <Card style={{minWidth: 300}}>
                        <Form
                            requiredMark={false}
                            layout="vertical"
                            name="basic"
                            initialValues={{remember: true}}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[{required: true, message: 'Please input your username!'}]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{required: true, message: 'Please input your password!'}]}
                            >
                                <Input.Password/>
                            </Form.Item>

                            <Form.Item name="remember" valuePropName="checked">
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Login
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Space>

        </Layout>
    );
};

export default Login;
