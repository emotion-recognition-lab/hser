import {Form, Input, Button, Checkbox, Card, Row, Col, Layout, Space, message, Tag, Spin} from 'antd';
import React, {useEffect, useState} from "react";
import axios from "axios";
import './../../styles/global.less';
import Head from "next/head";
import Wrapper from "../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import { Table } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import UserModel from "../../components/userModel";


const UserListPage = () => {

    const [users, setUsers] = useState([]);
    const [user, setUser] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isFormOpen, setIsFormOpen] = useState(false);

    const columns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'permissions',
            dataIndex: 'permissions',
            key: 'permissions',
            render: permissions => (
                <span>
                {permissions.map(permission => {
                    let color = 'green';
                    if (permission === 'loser') {
                        color = 'green';
                    }
                    return (
                        <Tag color={color} key={permission}>
                            {permission.toUpperCase()}
                        </Tag>
                    );
                })}
            </span>
            )
        },
        {
            title: <Button shape={"round"}  onClick={() => {setIsFormOpen(true)}}>Create New Account</Button>,
            dataIndex: 'username',
            key: 'username',
            render: username => (
                <span>
                <Button shape="circle" onClick={() => {openEditModal(username)}}>
                    <EditOutlined />
                </Button>
            </span>
            )
        },
    ];

    const openEditModal = (username) => {
        const userObj = users.find(u => u.username === username);
        setUser(userObj);
        setIsFormOpen(true);
    }
    const onCreate = () => {
        setIsFormOpen(false);
        setUser(null);
        axios.get('/api/user/users')
            .then((resp) => {
                setUsers(resp.data.users);
            }).catch((e) => {
            console.log(e);
            message.error('Cannot fetch data');
        }).finally(()=>{
            setIsLoading(false);
        });

    }

    useEffect(() => {
        axios.get('/api/user/users')
            .then((resp) => {
                setUsers(resp.data.users);
            }).catch((e) => {
            console.log(e);
            message.error('Cannot fetch data');
        }).finally(()=>{
            setIsLoading(false);
        });

    }, [])

    return (
        <>
            <Head>
                <title>User Management</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <Wrapper breadcrumb={[{href: '/sysinfo', title: 'User Management'}]}>
                <Content className={'index'}>
                    <Spin spinning={isLoading}>
                        <Table columns={columns} dataSource={users} />
                    </Spin>
                </Content>
            </Wrapper>
            {isFormOpen && <UserModel
                visible={isFormOpen}
                onCreate={onCreate}
                user={user}
                onCancel={() => {
                    setIsFormOpen(false);
                    setUser(null);
                }}/>
            }
        </>
    );
};

export default UserListPage;
