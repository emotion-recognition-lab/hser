import axios from "axios";

const SERVICE_URL = process.env.SERVICE_URL

export default async function profileHandler(req, res) {
    switch (req.method) {
        case 'GET':
            try {
                const resp = await axios.get(`${SERVICE_URL}/live`, {
                    timeout: 100
                });
                res.status(200).json(resp.data);
            } catch (e) {
                res.status(400);
            }

           break;
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}
