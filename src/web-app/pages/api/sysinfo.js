import sysInfo from "../../lib/sysinfo";

let cacheInfo = {}
let cacheTime = Date.now();

export default async function profileHandler(req, res) {
    switch (req.method) {
        case 'GET':
            if (cacheTime > Date.now()) {
                res.status(200).send({...cacheInfo, cache: true});
            } else {
                await sysInfo()
                    .then((info) => {
                        cacheTime = Date.now() + 2000;
                        cacheInfo = info;
                        res.status(200).send({...info, cache: false, cacheTime: cacheTime})
                    })
            }
            break;
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}
