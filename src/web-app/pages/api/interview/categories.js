
import nextConnect from 'next-connect'
import auth from '../../../middleware/auth'
import db  from '../../../lib/db'

const handler = nextConnect()

handler
    .use(auth)
    .get(async (req, res) => {
        const categories = await db.getInterviewCategories()
        res.send({categories})
    })
    .put(async(req, res) => {
        if (!req.user.permissions.includes('admin')) {
            res.status(401).send('unauthenticated')
        }
        const { title, details } = req.body
        const created_by = req.user.id;
        const category = await db.addInterviewCategory(title, details, created_by)
        res.send(category)
    })
    .patch(async(req, res) => {
        if (!req.user.permissions.includes('admin')) {
            res.status(401).send('unauthenticated')
        }
        const { id, title, details } = req.body
        const created_by = req.user.id;
        const category = await db.updateInterviewCategories(id, title, details, created_by)
        res.send(category)
    })

export default handler
