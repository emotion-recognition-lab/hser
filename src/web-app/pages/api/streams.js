import db from "../../lib/db";
import auth from "../../middleware/auth";
import nextConnect from "next-connect";

const handler = nextConnect()

handler
    .use(auth)
    .use((req, res, next) => {
        // handlers after this (PUT, DELETE) all require an authenticated user
        // This middleware to check if user is authenticated before continuing
        // console.log(req)
        if (!req.user) {
            res.status(401).send('unauthenticated')
        } else {
            next()
        }
    })
    .get((req, res) => {
        const {
            user,
        } = req;
        // Get data from database
        db.getAllStreams(user.id, user.permissions.includes('admin'))
            .then((sessions) => {
                res.status(200).json(sessions);
            })
    });
export default handler;
