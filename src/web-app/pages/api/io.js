import Socket from 'socket.io'

const ioHandler = (req, res) => {
  // console.log(res.socket);

  // if (!res.socket.server.io) {
  //   console.log('*First use, starting socket.io')
  //   const io = Socket(res.socket.server)

  //   io.on("error", e => console.log(e));

  //   let broadcaster

  //   io.on('connection', (socket) => {
  //     console.log('a user connected');
  //   });

  //   io.sockets.on("connection", socket => {
  //     console.log('connection')
  //     socket.on("broadcaster", () => {
  //       broadcaster = socket.id;
  //       socket.broadcast.emit("broadcaster");
  //     });
  //     socket.on("watcher", () => {
  //     console.log('watcher')

  //       socket.to(broadcaster).emit("watcher", socket.id);
  //     });
  //     socket.on("disconnect", () => {
  //       console.log('dis');
  //       socket.to(broadcaster).emit("disconnectPeer", socket.id);
  //     });
  //   });

  //   res.socket.server.io = io
  // } else {
  //   console.log('socket.io already running')
  // }
  // res.end()
}

export const config = {
  api: {
    bodyParser: false
  }
}

export default ioHandler
