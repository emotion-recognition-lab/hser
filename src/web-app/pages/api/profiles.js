import db from '../../lib/db';
import nextConnect from "next-connect";
import auth from "../../middleware/auth";

const handler = nextConnect()

handler
    .use(auth)
    .use((req, res, next) => {
        // handlers after this (PUT, DELETE) all require an authenticated user
        // This middleware to check if user is authenticated before continuing
        // console.log(req)
        if (!req.user) {
            res.status(401).send('unauthenticated')
        } else {
            next()
        }
    })
    .get((req, res) => {
        db.getAllProfile(req.user.id, req.user.permissions.includes('admin'))
            .then((profiles) => {
                res.status(200).send(profiles);
            })
    })

export default handler;
