import nextConnect from 'next-connect'
import auth from '../../../middleware/auth'
import {getAllUsers, createUser, findUserByUsername, updateUserById} from '../../../lib/authDB'
import db from "../../../lib/db";

const handler = nextConnect()

handler
    .use(auth)
    .get(async (req, res) => {
        if (!req.user.permissions.includes('admin')){
            res.statusCode(400).json({});
            return;
        }
        const users = await getAllUsers(req);
        const userList = users.map(u => {
            delete u.password;
            delete u.salt;
            delete u.hash;
            return u;
        });
        res.send({ users:userList })
    })
    .post(async (req, res) => {
        const { username, password, ...rest } = req.body
        if (!username || !password ) {
            return res.status(400).send('Missing fields')
        }
        // Here you check if the username has already been used
        const usernameExisted = !! await findUserByUsername(username)
        if (usernameExisted) {
            return res.status(409).send(`The username has already been used`)
        }

        const user = await createUser(req, user)
        rest.user_id = user.id;
        await db.createUserProfileByUserid(rest)

        res.status(201).send({user});
    })
    .put(async (req, res) => {
        const { username, password, id, isAdmin, ...rest } = req.body;

        if (!req.user.permissions.includes('admin') && req.user.id !== id) {
            res.status(400).end();
            return;
        }

        if (!username) {
            return res.status(400).send('Missing fields')
        }
        // Here you check if the username has already been used
        const usernameExisted = await findUserByUsername(username)
        if (!id && usernameExisted) {
            return res.status(409).send(`The username has already been used`)
        }
        let permissions  = ['operator'];

        if (isAdmin && req.user.permissions.includes('admin')){
            permissions.push('admin');
        }

        if (isAdmin === undefined) {
            permissions = usernameExisted.permissions;
        }

        if (id) {
            const user = await updateUserById(id, {username, password, id, permissions});
            delete user.password;
            delete user.salt;
            delete user.hash;
            rest.user_id = id;
            if (await db.findUserProfileByUserid(id)) {
                await db.updateUserProfileByUserid(rest)
            } else {
                await db.createUserProfileByUserid(rest)
            }
            return res.status(201).send({user});

        } else {
            const user = await createUser(req, { username, password, permissions });
            delete user.password;
            delete user.salt;
            delete user.hash
            rest.user_id = user.id;
            await db.createUserProfileByUserid(rest);
            return res.status(201).send({user});
        }
    })

export default handler
