
import nextConnect from 'next-connect'
import auth from '../../../middleware/auth'
import { deleteUser, updateUserById, findUserByUsername } from '../../../lib/authDB'

const handler = nextConnect()

handler
    .use(auth)
    .use((req, res, next) => {
        // handlers after this (PUT, DELETE) all require an authenticated user
        // This middleware to check if user is authenticated before continuing
        // console.log(req)
        if (!req.user) {
            res.status(401).send('unauthenticated')
        } else {
            next()
        }
    })
    .get(async (req, res) => {
        // You do not generally want to return the whole user object
        // because it may contain sensitive field such as !!password!! Only return what needed

        const { username } = req.user
        // res.json({ user: { name, username, favoriteColor } })
        const user = await findUserByUsername(username);

        res.send(user)
    })
    .put(async(req, res) => {
        const { name } = req.body
        const user = await updateUserById(req, req.user.id, { name })
        res.json({ user })
    })
    .delete((req, res) => {
        deleteUser(req)
        req.logOut()
        res.status(204).end()
    })

export default handler
