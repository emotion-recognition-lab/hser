import nextConnect from 'next-connect'
import auth from '../../../middleware/auth'
import {getAllUsers, createUser, findUserByUsername, updateUserById} from '../../../lib/authDB'
import db from "../../../lib/db";

const handler = nextConnect()

handler
    .use(auth)
    .post(async (req, res) => {
        const {userProfileData } = req.body
        if (!userProfileData.name || !userProfileData.family || !userProfileData.user_id ) {
            return res.status(400).send('Missing fields')
        }
        // Here you check if the username has already been used
        const userProfile = !! await db.findUserProfileByUserid(username)
        if (userProfile) {

        } else {

        }

        res.status(201);
    })

export default handler
