export default function profileHandler(req, res) {
    switch (req.method) {
        case 'GET':
            // Get data from your database
            res.status(200).send({
                active_features : {
                    NEXT_PUBLIC_UPLOAD_AUDIO: process.env.NEXT_PUBLIC_UPLOAD_AUDIO,
                    NEXT_PUBLIC_UPLOAD_VIDEO: process.env.NEXT_PUBLIC_UPLOAD_VIDEO,
                    NEXT_PUBLIC_UPLOAD_TEXT: process.env.NEXT_PUBLIC_UPLOAD_TEXT,
                    NEXT_PUBLIC_LIVE_VIDEO: process.env.NEXT_PUBLIC_LIVE_VIDEO
                }
            });

           break;
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}
