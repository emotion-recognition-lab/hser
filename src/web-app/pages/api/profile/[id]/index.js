import db from '../../../../lib/db';
import nextConnect from "next-connect";
import auth from "../../../../middleware/auth";

const fs = require('fs');
const path = require('path');
const dir = path.resolve('./upload');

const handler = nextConnect()

handler
    .use(auth)
    .get((req, res) => {
        const {
            query: {
                id
            },
            user
        } = req;

        if (!id) {
            res.status(400).send({})
        }
        // Get data from database
        db.getProfile(id, user.id, user.permissions.includes('admin'))
            .then(async (profile) => {
                const streams = await db.getAllSessions(id, user.id, user.permissions.includes('admin'));
                const userPath = path.join(dir, profile.id.toString() + '/snapshots');
                const snapshots = fs.existsSync(userPath) ? fs.readdirSync(userPath).filter(name => name.includes('.jpg')).map(name => `upload/${profile.id}/snapshots/${name}`) : [];
                res.status(200).send({
                    ...profile,
                    streams,
                    snapshots
                });
            })
    })
    .put((req, res) => {
        const {
            body: {name, family, birthday, gender, national_id},
            user
        } = req;
        db.addProfile(name, user.id, family,gender, birthday, national_id)
            .then((profile) => {
                res.status(200).send(profile)
            })

    });

export default handler;
