import db from "../../../../lib/db";

export default function recordsHandler(req, res) {
    const {
        query: {
            id
        },
        body: { name },
        method,
    } = req;

    switch (req.method) {
      case 'GET':
        // Get data from your database
        db.getAllProfileStreams(id)
        .then((records) => {
            res.status(200).json(records)
        })
        break
      default:
        res.setHeader('Allow', ['GET'])
        res.status(405).end(`Method ${req.method} Not Allowed`)
    }
  }
