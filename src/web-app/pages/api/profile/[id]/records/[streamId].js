import db from '../../../../../lib/db';
import nextConnect from "next-connect";
import auth from "../../../../../middleware/auth";
const fs = require('fs');
const path = require('path');
const dir = path.resolve('./upload');

const handler = nextConnect()

handler
    .use(auth)
    .get((req, res) => {
        const {
            query: {
                id,
                streamId
            },
            user,
        } = req;
        if (!id) {
            res.status(400).json({})
        }
        // Get data from database
        db.getAllProfileStreamsRecords(id, streamId, user.id, user.permissions.includes('admin'))
            .then((records) => {
                res.status(200).json(records);
            })
    });
export default handler;
