import db from "../../../../../lib/db";
import auth from "../../../../../middleware/auth";
import nextConnect from "next-connect";

const handler = nextConnect()

handler
    .use(auth)
    .get((req, res) => {
        const {
            query: {
                id,
                streamId
            },
            user,
        } = req;
        if (!id) {
            res.status(400).json({})
        }
        // Get data from database
        db.getSession(id, streamId, user.id, user.permissions.includes('admin'))
            .then((session) => {
                res.status(200).json(session);
            })

    })
    .delete((req, res) => {
        const {
            query: {
                id,
                streamId
            },
            user,
        } = req;

        db.getSession(id, streamId, user.id, user.permissions.includes('admin'))
            .then((session) => {
                if(session) {
                    db.deleteSessionById(streamId)
                        .then(() => {
                            res.status(200).json({});

                        })
                }
            })
    })
export default handler;
