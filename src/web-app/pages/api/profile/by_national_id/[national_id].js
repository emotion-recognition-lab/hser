import db from "../../../../lib/db";

export default function recordsHandler(req, res) {
    const {
        query: {
            national_id
        }
    } = req;

    switch (req.method) {
        case 'GET':
            // Get data from your database
            db.getProfileByInternationalId(national_id)
                .then((record) => {
                    res.status(200).json(record)
                })
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}
