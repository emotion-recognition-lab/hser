import nextConnect from 'next-connect'
import auth from '../../../../middleware/auth'
import db from '../../../../lib/db'

const emotions = ['Angry', 'Disgust', 'Fear', 'Happy', 'Neutral', 'Sad', 'Surprise'];
const factors = ['video', 'voice', 'text'];

const handler = nextConnect()
const defaultRatios = {
    [emotions[0]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[1]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[2]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[3]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[4]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[5]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    },
    [emotions[6]]: {
        [factors[0]]: 1,
        [factors[1]]: 1,
        [factors[2]]: 1,
    }
};
handler
    .use(auth)
    .get(async (req, res) => {
        const ratios = await db.getConfigById('multi-factor-ratio');
        if (ratios && ratios.value) {
            res.send(ratios)
        } else {
            await db.deleteConfigById('multi-factor-ratio');
            await db.insertConfig('multi-factor-ratio', 'multi-factor-ratio', defaultRatios);
            res.send({value: defaultRatios})
        }
    })
    .post(async (req, res) => {
        if (!req.user.permissions.includes('admin')) {
            res.status(401).send('unauthenticated')
        }
        const {ratio} = req.body
        await db.setConfigById('multi-factor-ratio', 'multi-factor-ratio', ratio)
        res.send({value: ratio})
    })


export default handler
