import Head from "next/head";
import Wrapper from "../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import {Card, Col, Divider, Row, Space, Typography} from "antd";
import React, {useState} from "react";
import "./style.less"
import {Image} from 'antd';

export default function sysInfo() {

    return (
        <>
            <Head>
                <title>Help</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Wrapper breadcrumb={[{href: '/help', title: 'Help'}]}>
                <Content className={'index'}>
                    <Card title={`Help`} style={{marginBottom: 16}} dir={"rtl"} className={"help"}>
                        <Typography.Title level={4}>
                            راهنمای استفاده از برنامه تحت وب آزمایشگاه شناسایی چند وجهی احساسات انسانی
                        </Typography.Title>
                        <Typography.Paragraph>
                            جهت بهره برداری از مدل های طراحی شده، برنامه تحت وب به ادرس زیر طراحی و پیاده سازی گردید که
                            می توانید با اسکن کردن بارکد زیر به لینک ورود برنامه دسترسی داشته و یا با آدرس
                            Emotion.shosseini.com به صفحه ورود به برنامه دسترسی داشته باشید. برای ورود به نام کاربری و
                            کلمه عبور نیاز می باشد که با ارسال درخواست نام کاربری و کلمه عبور به آدرس ایمیل
                            sadeghhosseini@malayeru. ac. ir در اختیار شما قرار خواهد گرفت.
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image2.png"} width={200}/>
                        </div>
                        <Divider/>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image5.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            بعد از ورود نام کاربری و کلمه عبور به صفحه اصلی برنامه وارد می شوید. دسترسی به بخش های مختلف
                            برنامه با توجه به نوع کاربری متفاوت است. با ورود نام کاربری با دسترسی سطح کاربر می توانید به
                            عنوان اپراتور پروفایل های مختلفی جهت آنالیز بسازید که بعد از وارد کردن مشخصات افراد و کد ملی
                            و یا شماره یکتا برای فرد به آرشیو پروفایلهایی که از قبل هم ساخته اید دسترسی داشته باشید. بعد
                            از ساخت پروفایل و ورود به آن انواع حالت برای آنالیز در دسترس شما خواهد گرفت که می توانید از
                            ورود اطلاعات متن، صوت و یا فیلم و یا آنالیز زنده استفاده نمایید.
                        </Typography.Paragraph>
                        <Divider/>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image4.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            همچنین می توانید مانند سایر نرم افزارها از عملیات جستجو و حذف و . . . استفاده نمایید.
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image7.jpg"} width={500}/>
                        </div>
                        <Divider/>
                        <Typography.Paragraph>
                            بعد از انتخاب نوع داده ورودی که می تواند یصورت متن، صوت ،تصویر و یا فیلم باشد، عملیات آنالیز
                            انجام شده و نتیجه قایل مشاهده خواهد بود. برای داده فیلم ابتدا فیلم آپلود شده به صوت و تصویر
                            تبدیل شده و همچنین صوت به متن تبدیل شده و برای هر داده عملیات آنالیز بر طبق مدل های تک وجهی
                            طراحی شده انجام می پذیرد. سپس در صورت وجود دو و یا سه نوع داده برای هر آنالیز، مدل های متن و
                            صوت و همچنین مدل صوت و تصویر و یا مدل متن و صوت و تصویر آنالیز مربوطه انجام شده و خروجی های
                            هر نوع داده بصورت تک وجهی و یا چند وجهی در منوی گزارش ها و یا لینک اختصاصی مربوط به داده
                            مورد نظر در اختیار اپراتور قرار داده خواهد شد. در زیر نمونه هایی از خروجی های تک وجهی صوت،
                            متن و یا تصویر و همچنین نمودار مقایسه ای هر مدل دو وجهی و یا مدل سه وجهی برای آشنایی بیشتر
                            آمده است. همچنین می توان در منوی گزارش از آنالیز لحظه ای در هر فریم و یا زمان مورد نظر با
                            انتخاب بازه زمانی بصورت دقیق در بازه مورد نظر دسترسی داشت.
                        </Typography.Paragraph>
                        <Typography.Paragraph>
                            نمونه خروجی آنالیز تصویر برای هر فریم از فیلم و مشخص شدن میزان حالات احساسی در هر فریم:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image6.png"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه خروجی آنالیز فیلم و مشخص شدن میزان سهم هر احساس در طول فیلم:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image9.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            می توان با دقیق شدن در بازه زمانی مشخص به جزئیات آنالیز احساسی برای هر فریم دسترسی پیدا
                            نمود. که در شکل زیر نمونه آن آمده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>

                            <Image src={"/help/image8.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه آنالیز صوتی که با استفاده از ایموجی در بازه های زمانی و هم بصورت نمایش سهم هر حالت
                            احساسی در کل فایل بصورت نمودار نمایش داده می شود:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image12.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه آنالیز احساسی متن که بصورت نمودار برای متن آپلود شده و یا استخراج شده از صوت و یا فیلم
                            نمایش داده شده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image10.png"} width={'300'}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه خروجی تشخیص حالت احساسی در موقع بیان کلمه مشخص و همچنین تصویر زمان بیان کلمه:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image11.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه خروجی مدل ترکیبی صوت و متن که با قراردان نشانگر موس بر روی نمودار، میزان سهم احساسی
                            متن و صوت و همچنین میزان محاسبه مدل ترکیبی طراحی شده مدل پیشنهادی با استفاده از عدد و گرافیک
                            نشان داده شده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image13.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه خروجی مدل ترکیبی صوت و تصویر که با قراردان نشانگر موس بر روی نمودار، میزان سهم احساسی
                            چهره و صوت و همچنین میزان محاسبه مدل ترکیبی طراحی شده مدل پیشنهادی با استفاده از عدد و
                            گرافیک نشان داده شده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image14.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            نمونه خروجی مدل ترکیبی صوت و متن و تصویر که با قراردان نشانگر موس بر روی نمودار، میزان سهم
                            احساسی متن و صوت و تصویر همچنین میزان محاسبه مدل ترکیبی طراحی شده مدل پیشنهادی با استفاده از
                            عدد و گرافیک نشان داده شده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image15.jpg"} width={500}/>
                        </div>
                        <Typography.Paragraph>
                            همچنین بخش جداگانه ای از موضوع این رساله، برای آنالیز شناسایی شخصیت افراد با استفاده از روش
                            های آنالیز متن با استفاده از متن آپلود شده و یا استخراج شده از صوت و یا فیلم و یا دریافت
                            آنلاین انجام گرفته که نمونه خروجی آن در زیر اورده شده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image16.jpg"} width={500}/>
                        </div>
                        <Divider/>
                        <Typography.Title level={3}>
                            دیگر موارد
                        </Typography.Title>
                        <Typography.Paragraph>
                            نسخه پرتابل این برنامه بر روی Raspberry پیاده سازی شده و قابل بهره برداری در صنایع خودرو
                            سازی و حمل و نقل و همچنین بعنوان دستیار مشاور در جلسات مصاحبه و آنالیز سخنرانی ها و ... می
                            باشد که تصویر آن در زیر آمده است:
                        </Typography.Paragraph>
                        <div className={'image-wrapper'}>
                            <Image src={"/help/image17.jpg"} width={500}/>
                            <Image src={"/help/image18.jpg"} width={500}/>
                            <Image src={"/help/image19.jpg"} width={500}/>
                        </div>
                    </Card>
                </Content>
            </Wrapper>
        </>
    )
}
