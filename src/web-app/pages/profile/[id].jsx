import Axios from 'axios';
import Head from 'next/head'
import React, {useRef, useState} from 'react';
import './../../styles/global.less';
import {useRouter} from 'next/router';
import Socket from './../../lib/useSocket'

import Layout, {Content} from "antd/lib/layout/layout";
import {Card, Col, Row, Space} from "antd";
import {UploadVideo} from "../../components/video-upload";
import TextUpload from "../../components/text-upload";
import LiveInterview from "../../components/live-interview";
import {VoiceVideo} from "../../components/voice-upload";
import IfActive from "../../components/IfActive";
import Wrapper from "../../components/wrapper";
import dynamic from "next/dynamic";
import {MehTwoTone} from "@ant-design/icons";
import {formatDistance} from "date-fns";

const StreamDetails = dynamic(
    () => import('../../components/stream-details'),
    {ssr: false}
)

export default function Profile() {
    const [profile, setProfile] = useState({});
    const [ENV, setENV] = useState({});
    const [showStreamDetails, setShowStreamDetails] = useState(null);

    let profileRef = useRef();
    const router = useRouter();
    profileRef.current = profile;


    React.useEffect(() => {
        if (!router.query.id) return;
        Axios.get(`/api/profile/${router.query.id}`)
            .then(res => {
                setProfile(res.data);
            });
        Axios.get(`/api/config`)
            .then(res => {
                setENV(res.data.active_features);
            });
        Socket.getInstance();


    }, [router.query.id]);

    const handleSetStreamDetails = (stream) => {
        router.push(`/profile/${router.query.id}/interview/${stream}`);
    };

    return (
        <>
            <Head>
                <title>Human Emotion & Sensitivity Laboratory - Labrador: {profile.name}</title>
                <link rel="icon" href="/icon.svg"/>
            </Head>
            <Wrapper breadcrumb={[{
                href: `/profile/${router.query.id}?category=${router.query.category}`,
                title: `${profile?.name} ${profile?.family ? profile?.family : ''}`
            }]}>
                <Layout>
                        <Card style={{marginBottom: 20}}>
                            <table  width={'100%'}>
                                <td>
                                    <tr>
                                        <b>First Name:</b> {profile?.name}
                                    </tr>
                                </td>
                                <td>
                                    <tr>
                                        <b>Last Name:</b> {profile?.family}
                                    </tr>
                                </td>
                                <td>
                                    <tr>
                                        <b>Gender:</b> {profile?.gender === 0 && 'Male'}{profile?.gender === 1 && 'Female'}
                                    </tr>
                                </td>
                                <td>
                                    <tr>
                                        <b>Age:</b> {profile?.birthday && formatDistance(new Date(profile?.birthday), new Date())}
                                    </tr>
                                </td>
                            </table>
                        </Card>

                        <Content>
                        {showStreamDetails &&
                        <Card>
                            <StreamDetails stream={showStreamDetails} profileId={profile.id}/>
                        </Card>
                        }
                        {!showStreamDetails &&
                        <>
                            <Row className={'new-interview-items'} gutter={24} align={"center"} justify={'center'}>
                                <Col span={8}>
                                    <IfActive active={ENV.NEXT_PUBLIC_LIVE_VIDEO=== 'true'}>
                                        <LiveInterview
                                            profileId={profileRef.current.id}
                                            onDone={handleSetStreamDetails}
                                        />
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={ENV.NEXT_PUBLIC_UPLOAD_VIDEO=== 'true'}>
                                        <Card>
                                            <UploadVideo
                                                profileId={profileRef.current.id}
                                                onDone={handleSetStreamDetails}
                                            />
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={ENV.NEXT_PUBLIC_UPLOAD_AUDIO === 'true'}>
                                        <Card>
                                            <VoiceVideo
                                                profileId={profileRef.current.id}
                                                onDone={handleSetStreamDetails}
                                            />
                                        </Card>
                                    </IfActive>
                                </Col>

                                <Col span={8}>
                                    <IfActive active={ENV.NEXT_PUBLIC_UPLOAD_TEXT=== 'true'}>
                                        <TextUpload
                                            profileId={profileRef.current.id}
                                            onDone={handleSetStreamDetails}
                                        />
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <MehTwoTone  style={{fontSize: 36}}/>
                                                    </div>
                                                    <p className={'title-text'}>Facial Skin Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img alt={''} src={'/eye.png'} width={42}/>
                                                    </div>
                                                    <p className={'title-text'}>Eye Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img alt={''} src={'/walk.png'} width={42}/>
                                                    </div>
                                                    <p className={'title-text'}>Walk Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>

                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img alt={''} src={'/brain.png'} width={42}/>
                                                    </div>
                                                    <p className={'title-text'}>Eeg Signal Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img alt={''} src={'/ecg.png'} width={42}/>
                                                    </div>
                                                    <p className={'title-text'}>ECG Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img alt={''} src={'/body-language.png'} width={42}/>
                                                    </div>
                                                    <p className={'title-text'}>Body Language Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img data-v-a26c9f3a="" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCAxNzIgMTcyIj48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgyNS44LDI1LjgpIHNjYWxlKDAuNywwLjcpIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9Im5vbnplcm8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBzdHJva2UtbGluZWNhcD0iYnV0dCIgc3Ryb2tlLWxpbmVqb2luPSJtaXRlciIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2UtZGFzaGFycmF5PSIiIHN0cm9rZS1kYXNob2Zmc2V0PSIwIiBmb250LWZhbWlseT0ibm9uZSIgZm9udC13ZWlnaHQ9Im5vbmUiIGZvbnQtc2l6ZT0ibm9uZSIgdGV4dC1hbmNob3I9Im5vbmUiIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTogbm9ybWFsIj48cGF0aCBkPSJNMCwxNzJ2LTE3MmgxNzJ2MTcyeiIgZmlsbD0ibm9uZSI+PC9wYXRoPjxnPjxwYXRoIGQ9Ik01NS45LDYuNDVjLTguMzExODksMCAtMTUuMDUsNi43MzgxMSAtMTUuMDUsMTUuMDVjMCw4LjMxMTg5IDYuNzM4MTEsMTUuMDUgMTUuMDUsMTUuMDVjOC4zMTE4OSwwIDE1LjA1LC02LjczODExIDE1LjA1LC0xNS4wNWMwLC04LjMxMTg5IC02LjczODExLC0xNS4wNSAtMTUuMDUsLTE1LjA1eiIgZmlsbD0iI2RmZjBmZSI+PC9wYXRoPjxwYXRoIGQ9Ik01NS45LDguNmM3LjExMjIsMCAxMi45LDUuNzg3OCAxMi45LDEyLjljMCw3LjExMjIgLTUuNzg3OCwxMi45IC0xMi45LDEyLjljLTcuMTEyMiwwIC0xMi45LC01Ljc4NzggLTEyLjksLTEyLjljMCwtNy4xMTIyIDUuNzg3OCwtMTIuOSAxMi45LC0xMi45TTU1LjksNC4zYy05LjQ5ODcsMCAtMTcuMiw3LjcwMTMgLTE3LjIsMTcuMmMwLDkuNDk4NyA3LjcwMTMsMTcuMiAxNy4yLDE3LjJjOS40OTg3LDAgMTcuMiwtNy43MDEzIDE3LjIsLTE3LjJjMCwtOS40OTg3IC03LjcwMTMsLTE3LjIgLTE3LjIsLTE3LjJ6IiBmaWxsPSIjNDc4OGM3Ij48L3BhdGg+PHBhdGggZD0iTTQwLjg1LDE2NS41NXYtNTAuNjAyNGwtMTIuOSwtOC42di0zNy41NDc2YzAsLTEzLjI2MTIgMTAuNTkwOSwtMjMuNjUgMjQuMTEwMSwtMjMuNjVoNy42Nzk4YzEzLjI5NTYsMCAyNC4xMTAxLDEwLjk1NjQgMjQuMTEwMSwyNC40MTk3djM2Ljc4MjJsLTEyLjksOC42djUwLjU5ODF6IiBmaWxsPSIjYjZkY2ZlIj48L3BhdGg+PHBhdGggZD0iTTU5LjczOTksNDcuM2MxMi4xMDg4LDAgMjEuOTYwMSw5Ljk4ODkgMjEuOTYwMSwyMi4yNjk3djM1LjYyOThsLTEwLjk4NjUsNy4zMjI5bC0xLjkxMzUsMS4yNzcxdjIuMzAwNXY0Ny4zaC0xMy4yMDUzaC0xMi41OTQ3di00Ny4zdi0yLjMwMDVsLTEuOTEzNSwtMS4yNzcxbC0xMC45ODY1LC03LjMyMjl2LTM2LjM5OTVjMCwtMTIuMDU3MiA5LjY0NDksLTIxLjUgMjEuOTYwMSwtMjEuNWg3LjY3OThNNTkuNzM5OSw0M2gtNy42Nzk4Yy0xNC41MDM5LDAgLTI2LjI2MDEsMTEuMjk2MSAtMjYuMjYwMSwyNS44djM4LjdsMTIuOSw4LjZ2NTEuNmgxNi44OTQ3aDE3LjUwNTN2LTUxLjZsMTIuOSwtOC42di0zNy45MzAzYzAsLTE0LjUwMzkgLTExLjc1NjIsLTI2LjU2OTcgLTI2LjI2MDEsLTI2LjU2OTd6IiBmaWxsPSIjNDc4OGM3Ij48L3BhdGg+PHBhdGggZD0iTTkyLjM1NTQsMzQuNDc3NGMxLjQ3OTIsLTQuMTQwOSAyLjI0NDYsLTguNTMxMiAyLjI0NDYsLTEyLjk3NzRjMCwtNS4xOTg3IC0xLjA1NzgsLTEwLjE1MjMgLTIuOTMyNiwtMTQuNjgwMmMtMC42NDkzLC0xLjU1NjYgLTIuMjM2LC0yLjUxOTggLTMuOTIxNiwtMi41MTk4djBjLTMuMDc0NSwwIC01LjA5NTUsMy4xMDQ2IC0zLjkzODgsNS45NTU1YzEuNDEwNCwzLjQ3NDQgMi4xOTMsNy4yNjcgMi4xOTMsMTEuMjQ0NWMwLDUuNTQ3IC0xLjUxMzYsMTAuODQ4OSAtNC4yNzg1LDE1LjQzMjdjMTAuNTE3OCw3LjE4MSAxNy4xNzg1LDE5LjI2ODMgMTcuMTc4NSwzMi42Mzd2NDQuODM2MWwtMTIuOSw4LjZ2NDAuMzk0MmMwLDIuMzczNiAxLjkyNjQsNC4zIDQuMyw0LjN2MGMyLjM3MzYsMCA0LjMsLTEuOTI2NCA0LjMsLTQuM3YtMzUuNzkzMmwxMi45LC04LjZ2LTQuNjAxdi00NC44MzYxYzAsLTEzLjQyODkgLTUuNTU1NiwtMjYuMDg4MSAtMTUuMTQ0NiwtMzUuMDkyM3oiIGZpbGw9IiM0Nzg4YzciPjwvcGF0aD48ZyBmaWxsPSIjOThjY2ZkIj48cGF0aCBkPSJNMTE1LjY4MjksMjguNTc3OGMwLjI3OTUsLTIuMzQzNSAwLjQxNzEsLTQuNzEyOCAwLjQxNzEsLTcuMDc3OGMwLC00Ljc4MTYgLTAuNjIzNSwtOS40NjQzIC0xLjc1ODcsLTE0LjAzNTJjLTAuNDY4NywtMS44ODM0IC0yLjIxNDUsLTMuMTY0OCAtNC4xNTM4LC0zLjE2NDh2MGMtMi43NjQ5LDAgLTQuNzUxNSwyLjU4NDMgLTQuMTAyMiw1LjI3MThjMC45Mzc0LDMuODg3MiAxLjQxNDcsNy44NjkgMS40MTQ3LDExLjkyODJjMCwzLjE3MzQgLTAuMjg4MSw2LjMyNTMgLTAuODY0Myw5LjQxN2M4LjgxOTMsMTAuODIzMSAxMy43NjQzLDI0LjUyNzIgMTMuNzY0MywzOC42NTI3djU2LjM0MjlsLTEyLjksOC42djI4Ljg4NzRjMCwyLjM3MzYgMS45MjY0LDQuMyA0LjMsNC4zdjBjMi4zNzM2LDAgNC4zLC0xLjkyNjQgNC4zLC00LjN2LTI0LjI4NjRsMTIuOSwtOC42di02MC45NDM5YzAsLTE0LjcwNiAtNC43MDQyLC0yOS4xMTUzIC0xMy4zMTcxLC00MC45OTE5eiI+PC9wYXRoPjwvZz48ZyBmaWxsPSIjYjZkY2ZlIj48cGF0aCBkPSJNMTM3LjU5MTQsMjIuODAyOWMwLjAwNDMsLTAuNDM0MyAwLjAwODYsLTAuODY4NiAwLjAwODYsLTEuMzAyOWMwLC00LjU0NTEgLTAuNDE3MSwtOS4xMzMyIC0xLjIzODQsLTEzLjcyMTNjLTAuMzYxMiwtMi4wMjUzIC0yLjE4MDEsLTMuNDc4NyAtNC4yMzU1LC0zLjQ3ODd2MGMtMi42NTc0LDAgLTQuNzE3MSwyLjM4MjIgLTQuMjQ4NCw0Ljk5NjZjMC43MzUzLDQuMDg5MyAxLjEyMjMsOC4xNyAxLjEyMjMsMTIuMjAzNGMwLDEuMTk5NyAtMC4wMzAxLDIuMzk5NCAtMC4wOTAzLDMuNjAzNGM4LjQwNjUsMTMuMjE4MiAxMi45OTAzLDI4LjgyMjkgMTIuOTkwMyw0NC40NjYzdjYwLjk0Mzl2Ni45MDU4bC01Ljc0NDgsMy44MzEzbC03LjE1NTIsNC43NjQ0djE3LjM4NDljMCwyLjM3MzYgMS45MjY0LDQuMyA0LjMsNC4zdjBjMi4zNzM2LDAgNC4zLC0xLjkyNjQgNC4zLC00LjN2LTEyLjc3OTZsMy4zMjM5LC0yLjIxODhsNS43NDQ4LC0zLjgzMTNsMy44MzEzLC0yLjU0OTl2LTQuNjAxdi02LjkwNTh2LTYwLjk0MzljMCwtMTYuMjE5NiAtNC41NzA5LC0zMi43MTQ0IC0xMi45MDg2LC00Ni43NjY4eiI+PC9wYXRoPjwvZz48L2c+PHBhdGggZD0iIiBmaWxsPSJub25lIj48L3BhdGg+PHBhdGggZD0iIiBmaWxsPSJub25lIj48L3BhdGg+PHBhdGggZD0iIiBmaWxsPSJub25lIj48L3BhdGg+PC9nPjwvZz48L3N2Zz4=" alt="icon" width="42" height="42" />
                                                    </div>
                                                    <p className={'title-text'}>Thermal Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>
                                <Col span={8}>
                                    <IfActive active={false}>
                                        <Card>
                                            <div className={'button-area'}>
                                                <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                                                    <div className={'icon'}>
                                                        <img data-v-a26c9f3a="" src="/breath.png" alt="icon" width="42" height="42" />
                                                    </div>
                                                    <p className={'title-text'}>Breath Analyzer</p>
                                                    <p className={'hint-text'}></p>
                                                </Space>
                                            </div>
                                        </Card>
                                    </IfActive>
                                </Col>

                            </Row>
                        </>
                        }
                    </Content>
                </Layout>


            </Wrapper>
        </>
    )
}
