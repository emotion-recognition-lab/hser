import Axios from 'axios';
import Head from 'next/head'
import React, {useRef, useState} from 'react';
import './../../../../styles/global.less';
import {useRouter} from 'next/router';

import Layout, {Content} from "antd/lib/layout/layout";
import {Card} from "antd";
import Wrapper from "../../../../components/wrapper";
import dynamic from "next/dynamic";

const StreamDetails = dynamic(
    () => import('../../../../components/stream-details'),
    {ssr: false}
)

export default function Profile() {
    const [profile, setProfile] = useState({});

    const router = useRouter();


    React.useEffect(() => {
        if (!router.query.id) return;
        Axios.get(`/api/profile/${router.query.id}`)
            .then(res => {
                setProfile(res.data);
            });

    }, [router.query.id]);

    return (
        <>
            <Head>
                <title>Human Emotion & Sensitivity Laboratory - Labrador: {profile.name} {profile.family}</title>
                <link rel="icon" href="/icon.svg"/>
            </Head>
            <Wrapper breadcrumb={[{
                href: `/profile/${router.query.id}/interview/${router.query.streamId}`,
                title: `${profile?.name} ${profile?.family ? profile?.family : ''}`
            }]}>
                <Layout>
                    <Content>
                        <Card>
                            <StreamDetails streamId={router.query.streamId} profileId={router.query.id}/>
                        </Card>
                    </Content>
                </Layout>


            </Wrapper>
        </>
    )
}
