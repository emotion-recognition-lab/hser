import Head from 'next/head'
import Profiles from '../components/profiles';
import ProfilesPage from "./../components/new-profile"
import './../styles/global.less';
import './index.less'
import {Card, Col, Space, Row} from "antd";
import Layout, {Footer, Content} from "antd/lib/layout/layout";
import Wrapper from "../components/wrapper";
import React from "react";
import StreamTable from "../components/stream-table";


export default function Home() {

    return (
        <>
            <Head>
                <title>Human Emotion & Sensitivity Laboratory</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <Wrapper>
                <Content className={'index'}>
                    <Card>
                        <Row align={'center'}>
                            <Col>
                                <Space align={'baseline'} target={'_blank'}>
                                    Get started by add <span><ProfilesPage/></span>
                                </Space>
                            </Col>
                        </Row>
                    </Card>
                    {/*<Profiles/>*/}
                    <StreamTable/>
                </Content>
            </Wrapper>
        </>
    )
}
