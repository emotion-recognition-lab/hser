import {Button, message, Tag, Spin, Avatar} from 'antd';
import React, {useEffect, useState} from "react";
import axios from "axios";
import './../../styles/global.less';
import Head from "next/head";
import Wrapper from "../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import { Table } from 'antd';
import {EditOutlined, UserOutlined} from '@ant-design/icons';
import InterviewCategoryModel from "../../components/interview-category";


const InterviewCategoriesListPage = () => {

    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isFormOpen, setIsFormOpen] = useState(false);

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Details',
            dataIndex: 'details',
            key: 'details',
            render: details => (
                <span>
                    {details}
                </span>
            )
        },
        {
            title: 'Created By',
            dataIndex: 'user',
            key: 'created_by',
            render: user => (
                <span>
                    <Avatar icon={<UserOutlined />} src={user && user.photo ? '/' + user.photo : null} /> {user.name} {user.family}
                </span>
            )
        },
        {
            title: <Button shape={"round"}  onClick={() => {setIsFormOpen(true)}}>Create New Category</Button>,
            dataIndex: 'id',
            key: 'id',
            render: id => (
                <span>
                <Button shape="circle" onClick={() => {openEditModal(id)}}>
                    <EditOutlined />
                </Button>
            </span>
            )
        },
    ];

    const openEditModal = (id) => {
        const catObj = categories.find(u => u.id === id);
        setCategory(catObj);
        setIsFormOpen(true);
    }
    const onCreate = () => {
        setIsFormOpen(false);
        setCategory(null);
        axios.get('/api/interview/categories')
            .then((resp) => {
                setCategories(resp.data.categories);
            }).catch((e) => {
            console.log(e);
            message.error('Cannot fetch data');
        }).finally(()=>{
            setIsLoading(false);
        });

    }

    useEffect(() => {
        axios.get('/api/interview/categories')
            .then((resp) => {
                setCategories(resp.data.categories);
            }).catch((e) => {
            console.log(e);
            message.error('Cannot fetch data');
        }).finally(()=>{
            setIsLoading(false);
        });

    }, [])

    return (
        <>
            <Head>
                <title>Interview Categories Management</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <Wrapper breadcrumb={[{href: '/sysinfo', title: 'Interview Categories Management'}]}>
                <Content className={'index'}>
                    <Spin spinning={isLoading}>
                        <Table columns={columns} dataSource={categories} />
                    </Spin>
                </Content>
            </Wrapper>
            {isFormOpen && <InterviewCategoryModel
                visible={isFormOpen}
                onCreate={onCreate}
                category={category}
                onCancel={() => {
                    setIsFormOpen(false);
                    setCategory(null);
                }}/>
            }
        </>
    );
};

export default InterviewCategoriesListPage;
