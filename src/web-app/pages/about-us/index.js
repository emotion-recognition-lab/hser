import Head from "next/head";
import Wrapper from "../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import {Card, Col, Row, Space} from "antd";
import React, {useState} from "react";


export default function aboutUs() {

    return (
        <>
            <Head>
                <title>About Us</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Wrapper breadcrumb={[{href: '/about-us', title: 'Help'}]}>
                <Content className={'index'}>
                    <Card title={`About Us`} style={{marginBottom: 16}}>
                        <h2>Multimodal Emotion Identification Laboratory</h2>
                        <p>
                            Video, Voice, Text, Image, Termal image, EEG signal, ECG signal, EMG signal, Facial skin,
                            Walking, Body language, Breathing, …
                            Implementation of the proposal for the doctoral course in artificial intelligence
                            <br/>
                            Islamic azad university - Lahijan branch
                            <br/>
                            Supervisor: <b>Dr. Moahamad Reza Yamaghani</b>
                            <br/>
                            Advisor: <b>Dr. Soudabeh Arabani</b>
                            <br/>
                            Student: <b>Seyed Sadegh Hosseini</b>

                        </p>
                    </Card>
                </Content>
            </Wrapper>
        </>
    )
}
