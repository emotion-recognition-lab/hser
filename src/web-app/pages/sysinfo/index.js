import Head from "next/head";
import Wrapper from "../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import {Card, Col, Row, Space} from "antd";
import ProfilesPage from "../../components/new-profile";
import Profiles from "../../components/profiles";
import React, {useState} from "react";
import Axios from "axios";
import Waffle from "../../components/charts/waffle";

export default function sysInfo() {
    const [sysInfo, setSysInfo] = useState({});
    const [ramInfo, setRamInfo] = useState([]);
    const [swapInfo, setSwapInfo] = useState([]);
    const [disKInfo, setDiskInfo] = useState([]);

    // JavaScript To Convert Bytes To MB, KB, Etc - https://gist.github.com/lanqy/5193417
    function bytesToHumanReadable(bytes) {
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if (bytes === 0) return 'n/a'
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
        if (i === 0) return `${bytes} ${sizes[i]}`
        return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
    }

    function bytesToSize(bytes) {
        if (bytes === 0) return 'n/a'
        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
        if (i === 0) return `${bytes}}`
        return `${(bytes / (1024 ** i)).toFixed(1)}`
    }


    React.useEffect(() => {
        const interval = setInterval(() => {
            Axios.get(`/api/sysinfo`)
                .then(res => {
                    setSysInfo(res.data);
                    setRamInfo([
                        {
                            "id": "Used Memory",
                            "label": "% Used Memory",
                            "value": parseInt((res.data.used_memory * 100 / res.data.total_memory),10),
                            "color": "#ba72ff"
                        },
                        {
                            "id": "Free Memory",
                            "label": `% Free Memory`,
                            "value": parseInt((res.data.free_memory * 100 / res.data.total_memory)),
                            "color": "#a1cfff"
                        }
                    ]);
                    setSwapInfo([
                        {
                            "id": "Used Swap Memory",
                            "label": "% Used Swap Memory",
                            "value": parseInt((res.data.total_swap_memory_used * 100 / res.data.total_swap_memory),10),
                            "color": "#ba72ff"
                        },
                        {
                            "id": "Free Swap Memory",
                            "label": `% Free Swap Memory`,
                            "value": parseInt((res.data.total_swap_memory_free * 100 / res.data.total_swap_memory)),
                            "color": "#a1cfff"
                        }
                    ]);
                    setDiskInfo([
                        {
                            "id": `Used Storage: ${bytesToHumanReadable(res.data.disk_size_used)}`,
                            "label": "% Used Storage",
                            "value": parseInt((res.data.disk_size_used * 100 / res.data.disk_size),10),
                            "color": "#ba72ff"
                        },
                        {
                            "id": `Free Storage: ${bytesToHumanReadable(res.data.disk_size - res.data.disk_size_used)}`,
                            "label": `% Free Storage`,
                            "value": 100 - parseInt((res.data.disk_size_used * 100 / res.data.disk_size),10),
                            "color": "#a1cfff"
                        }
                    ]);
                });


        }, 2000);
        return () => {
            clearInterval(interval);
        }
    }, []);

    return (
        <>
        <Head>
            <title>System Monitoring</title>
            <link rel="icon" href="/favicon.ico"/>
        </Head>

        <Wrapper breadcrumb={[{href: '/sysinfo', title: 'System Monitoring'}]}>
            <Content className={'index'}>
                <Card title={`Memory`} extra={bytesToHumanReadable(sysInfo.total_memory || 0)} style={{ marginBottom: 16 }}>
                    <Waffle data={ramInfo} totol={100}/>
                </Card>

                <Card title={`Swap Memory`} extra={bytesToHumanReadable(sysInfo.total_swap_memory || 0)} style={{ marginBottom: 16 }}>
                    <Waffle data={swapInfo} totol={100}/>
                </Card>

                <Card title={`Disk`} extra={bytesToHumanReadable(sysInfo.disk_size || 0)} style={{ marginBottom: 16 }}>
                    <Waffle data={disKInfo} totol={100}/>
                </Card>
            </Content>
        </Wrapper>
        </>
    )
}
