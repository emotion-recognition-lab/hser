import Head from "next/head";
import Wrapper from "../../../components/wrapper";
import {Content} from "antd/lib/layout/layout";
import {Button, Card, Col, Input, Row, Space, Table} from "antd";
import ProfilesPage from "../../../components/new-profile";
import Profiles from "../../../components/profiles";
import React, {useEffect, useState} from "react";
import Axios from "axios";
import Waffle from "../../../components/charts/waffle";

export default function ConfigMultiFactorRatio() {
    const [data, setData] = useState(null);

    useEffect(() => {
        Axios.get('/api/administration/configs/multi-factor-ratio')
            .then((response) => {
                setData(response.data.value)
            });
    }, []);

    useEffect(() => {
        console.log(data)
    }, [data]);

    return (
        <>
            <Head>
                <title>System Monitoring</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Wrapper breadcrumb={[{href: '/configuration/multi-factor-ratio', title: 'Configuration'}]}>
                <Content className={'index'}>
                    <Card title={`Multi Factor Ratios`}>
                        <div className={'ant-table'}>
                            {data &&
                                <>
                                    <table className={'ant-table-content'}>
                                        <thead>
                                        <tr>
                                            <th></th>
                                            {
                                                Object.keys(data).map((ratio, index) => {
                                                    return <th key={index}>{ratio}</th>
                                                })
                                            }
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            Object.keys(data[Object.keys(data)[0]]).map((factor, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td><b>{factor}</b></td>
                                                        {
                                                            Object.keys(data).map((ratio, index) => {
                                                                return (
                                                                    <td key={index}>
                                                                        <Input
                                                                            style={{width: 50}}
                                                                            defaultValue={data[ratio][factor]}
                                                                            onChange={(e) => {
                                                                                data[ratio][factor] = parseFloat(e.target.value);
                                                                                setData({...data});
                                                                            }}
                                                                        />
                                                                    </td>
                                                                )
                                                            })
                                                        }
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                    <div style={{display: 'flex', justifyContent: 'flex-end', margin: 20}}>
                                        <Button
                                            type={'primary'}
                                            onClick={() => {
                                                Axios.post('/api/administration/configs/multi-factor-ratio', {ratio: data})
                                                    .then((response) => {
                                                    });
                                            }
                                            }>
                                            Save
                                        </Button>
                                    </div>
                                </>
                            }
                        </div>
                    </Card>
                </Content>
            </Wrapper>
        </>
    )
}
