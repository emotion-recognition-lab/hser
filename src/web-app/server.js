const express = require('express')
const app = express()
// const server = require('https').Server(app)
const https = require('http');
const fs = require('fs-extra');
const fsp = require('fs');


const options = {
//     key: fsp.readFileSync('./example.key'),
//     cert: fsp.readFileSync('./example.crt'),
};
const server = https.createServer(options, app);const io = require('socket.io')(server)
const ffmpeg = require('fluent-ffmpeg');
const next = require('next')
const multer  = require('multer');
const upload = multer({ dest: 'uploads/' });

const port = parseInt(process.env.PORT, 10) || 4000
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({
    dev
})
const nextHandler = nextApp.getRequestHandler()
const uploadAuth = require('./middleware/uploadAuth');
const socketAuth = require('./middleware/socketAuth');
const socketLib = require('./lib/socketEvents');
console.log(process.env)
// initialize server
const db = require('./lib/db');
const path = require("path");
db.createTables();



// socket.io server
io.on('connection', socket => {
    socket.onAny(async (eventName, ...args) => {
        if (await socketAuth(socket)) {
            socketLib.socketEvents[eventName](socket, args[0]);
        } else {
            socket.emit('unauthenticated');
        }
    });
})

nextApp.prepare().then(() => {

    app.use('/upload', express.static(path.join(__dirname, './upload')));


    app.post('/file/upload', upload.single('file'), async function (req, res, next) {
        if (!await uploadAuth(req)) {
            res.status(400).end();
            return;
        }
        const fileFormat = req.file.originalname.split('.').slice(-1)[0];
        const fileName =`upload/${req.body.profileId}/${req.body.streamId}/${req.body.streamId}.${fileFormat}`;

        fs.moveSync(req.file.path, fileName);

        db.addSession(req.body.profileId, req.body.category ,req.body.streamId, fileName, '', req.user.id)
            .then((session) => {
                res.status(200).json({upload: 'ok' , session});
            })
    })


    app.post('/file/upload/sign', upload.single('sign'), async function (req, res, next) {
        const user = await uploadAuth(req);
        if (!user) {
            res.status(400).end();
            return;
        }
        const fileFormat = req.file.originalname.split('.').slice(-1)[0];
        const fileName =`upload/profiles/${user.id}/sign-${Date.now()}.${fileFormat}`;

        fs.moveSync(req.file.path, fileName);
        res.send({
            path: fileName
        })
    })


    app.post('/file/upload/photo', upload.single('photo'), async function (req, res, next) {
        const user = await uploadAuth(req);
        if (!user) {
            res.status(400).end();
            return;
        }
        const fileFormat = req.file.originalname.split('.').slice(-1)[0];
        const fileName =`upload/profiles/${user.id}/photo-${Date.now()}.${fileFormat}`;

        fs.moveSync(req.file.path, fileName);
        res.send({
            path: fileName
        })
    })



    app.all('*', (req, res) => {
        if (!req.io) {
            req.io = io;
        }
        return nextHandler(req, res)
    })


    server.listen(port, err => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
})
