
const db = require("../lib/db");
const Iron = require("@hapi/iron");
const parse = require("cookie").parse;


function parseCookies(req) {
    // For API Routes we don't need to parse the cookies.
    if (req.cookies) return req.cookies

    // For pages we do need to parse the cookies.
    const cookie = req.headers.cookie
    return parse(cookie || '')
}

const uploadAuth = async (req) => {
    const secret = process.env.TOKEN_SECRET;
    const cookies = parseCookies(req)
    const token = cookies['sess'];
    if (!token) {
        return false;
    }


    try {
        const session = await Iron.unseal(token, secret, Iron.defaults)
        const expiresAt = session.createdAt + session.maxAge * 1000

        // Validate the expiration date of the session
        if (session.maxAge && Date.now() < expiresAt) {
            const user = await db.findUserByUsername(session.passport.user);
            req.session = session;
            req.user = user;
            req.user.isAdmin = user.permissions.includes('admin');
            return user
        }
    } catch (e) {

    }
    return false
};

module.exports = uploadAuth;
