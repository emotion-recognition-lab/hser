

const db = require("../lib/db");
const Iron = require("@hapi/iron");

const socketAuth = async (socket) => {
    const secret = process.env.TOKEN_SECRET;
    const token = socket.handshake.auth.token;

    if (!token) {
        return false;
    }

    try {
        const session = await Iron.unseal(token, secret, Iron.defaults)
        const expiresAt = session.createdAt + session.maxAge * 1000

        // Validate the expiration date of the session
        if (session.maxAge && Date.now() < expiresAt) {
            const user = await db.findUserByUsername(session.passport.user);
            socket.session = session;
            socket.user = user;
            socket.user.isAdmin = user.permissions.includes('admin');
            return true
        }
    } catch (e) {

    }
    return false
};

module.exports = socketAuth;
