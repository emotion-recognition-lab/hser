import React, { useEffect, useState} from "react";
import {Row, Col, Dropdown, Menu, Space, Badge, Tag} from "antd";
import {Header as AntHeader} from "antd/lib/layout/layout";
import {UserOutlined, CheckCircleOutlined, SyncOutlined, CloseCircleOutlined} from '@ant-design/icons';
import axios from "axios";
import {useUser} from "../../lib/useUser";
import {useRouter} from "next/router";
import Avatar from "antd/lib/avatar/avatar";
import UserModel from "../userModel";

const Header = (props) => {
    const [serviceStatus, setServiceStatus] = React.useState(null);
    const router = useRouter()
    const [isFormOpen, setIsFormOpen] = useState(false);

    const {user, mutateUser} = useUser()
    const handleLogout = async () => {

        await fetch("/api/user/logout", { method: "get" });
        router.push("/user/login");
    }

    useEffect(() => {
        const interval = setInterval(() => {
            axios.get('/api/live', {
                timeout: 2000
            })
                .then((resp) => {
                    setServiceStatus(resp.data.live);
                })
                .catch(() => {
                    setServiceStatus(false);
                })
        }, 30000);
        return () => {
            clearInterval(interval);
        }
    }, [])

    const menu = (
        <Menu onClick={() => {
        }}>
            <Menu.Item key="1" onClick={() => {setIsFormOpen(true)}}>Edit Profile</Menu.Item>
            <Menu.Item key="2" onClick={handleLogout}>Logout</Menu.Item>
        </Menu>
    )

    return (
        <AntHeader style={{backgroundColor: 'transparent'}}>
            <Row gutter={16} justify="space-between">
                <Col>
                    <Space align="center">
                        {props.children}
                    </Space>
                </Col>
                <Col>
                    <Row gutter={16}>
                        <Col>
                            <div>
                                <Badge
                                    className="site-badge-count-109"
                                    style={{backgroundColor: '#52c41a'}}
                                >
                                    {serviceStatus === null &&
                                    <Tag color="#ccc" icon={<SyncOutlined spin/>}>Updating Service Status</Tag>
                                    }
                                    {serviceStatus === false &&
                                    <Tag color="#f50" icon={<CloseCircleOutlined/>}>Service Status is Down</Tag>
                                    }
                                    {serviceStatus === true &&
                                    <Tag color="#87d068" icon={<CheckCircleOutlined/>}>Service Status is Live</Tag>
                                    }

                                </Badge>
                            </div>
                        </Col>
                        <Col>
                            {user &&
                            <>{user.name} {user.family}
                            <Dropdown
                                arrow
                                overlay={menu}
                                >
                                  <a>
                                      <Avatar icon={<UserOutlined />} src={user && user.photo ? '/' + user.photo : null} /></a>
                                </Dropdown>
                            </>
                            }

                        </Col>
                    </Row>
                </Col>
            </Row>
            {isFormOpen && <UserModel
                disablePermission
                visible={isFormOpen}
                onCreate={() => {
                    setIsFormOpen(false);
                }}
                user={user}
                onCancel={() => {
                    setIsFormOpen(false);
                }}/>
            }
        </AntHeader>
    )
}

export default Header;
