import React, {useState} from 'react';
import {Divider, Modal, Form, Input, Radio,   Upload, Button} from 'antd';
import axios from "axios";
import { UploadOutlined, InboxOutlined } from '@ant-design/icons';

const UserModel = ({
                       visible,
                       onCreate,
                       onCancel,
                       user,
                       disablePermission
                   }) => {
    const [form] = Form.useForm();

    const uploadFile = (e) => {
        if (e.file.status === 'done') {
            return e.file.response.path;
        }
    };

    return (
        <Modal
            visible={visible}
            title={user ? "Edit User" : "Create a new user"}
            okText={user ? "Edit " : "Create"}
            cancelText="Cancel"
            onCancel={onCancel}
            destroyOnClose={true}
            onOk={() => {
                form
                    .validateFields()
                    .then(async values => {
                        values.id = user?.id;
                        try {
                            const resp = await axios.put('/api/user/users', values);
                            form.resetFields();
                            onCreate();
                        } catch (e) {
                            console.log(e)
                        }

                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
                initialValues={{isAdmin: user  && user.permissions.includes('admin'), ...user, password: ''}}
                hideRequiredMark={true}
            >
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[{required: true, message: 'Please input the name!'}]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="family"
                    label="Family"
                    rules={[{required: true, message: 'Please input the family!'}]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="national_id"
                    label="National Id"
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="email"
                    label="Email"
                >
                    <Input type={'email'}/>
                </Form.Item>
                <Form.Item
                    name="phone"
                    label="phone"
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    name="address"
                    label="Address"
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Description"
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    name="photo"
                    label="photo"
                    valuePropName="photo"
                    getValueFromEvent={uploadFile}
                >

                    <Upload name="photo" action="/file/upload/photo" listType="picture" maxCount={1}>
                        <Button icon={<UploadOutlined />}>Click to upload</Button>
                    </Upload>

                </Form.Item>
                <Form.Item
                    name="sign"
                    label="Sign"
                    valuePropName="sign"
                    getValueFromEvent={uploadFile}
                >
                    <Upload name="sign" action="/file/upload/sign" listType="picture" maxCount={1}>
                        <Button icon={<UploadOutlined />}>Click to upload</Button>
                    </Upload>
                </Form.Item>

                <Divider/>
                <Form.Item
                    name="username"
                    label="username"
                    rules={[{required: true, message: 'Please input the username!'}]}
                >
                    <Input disabled={!!user}/>
                </Form.Item>
                <Form.Item
                    name="password"
                    label="password"
                    rules={[{required: !user, message: 'Please input the password!'}]}
                >
                    <Input.Password/>
                </Form.Item>
                {!disablePermission &&
                <Form.Item name="isAdmin">
                    <Radio.Group>
                        <Radio value={false}>Operator</Radio>
                        <Radio value={true}>Admin</Radio>
                    </Radio.Group>
                </Form.Item>
                }

            </Form>
        </Modal>
    );
};

export default UserModel;
