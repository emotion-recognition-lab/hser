import Axios from "axios";
import React, {useCallback, useEffect, useState} from "react";
import Text from "antd/lib/typography/Text";
import {Alert, Col, Divider, Row, Tooltip} from "antd";
import {TextBarChart} from "../charts/text-bar";
import TextRadarChart from "../charts/radar";
import {format} from "date-fns";

function TextReport({textsItems, records}) {
    const [texts, setTexts] = useState([]);

    useEffect(() => {
        if (textsItems && textsItems.length > 0) {
            loadTexts(textsItems)
        }
    }, [textsItems.length]);

    const loadTexts = (textsArr) => {
        textsArr
            .forEach(async (txt, index) => {
                try {
                    await Axios.get(`/${txt.path}`)
                        .then((value) => {
                            const newTextObject = [...textsArr];

                            const txtObj = value.data;
                            if (txtObj.text) {
                                newTextObject[index] = {
                                    ...txt,
                                    text: txtObj.text,
                                    meta: txtObj.result.map(r => ({
                                        ...r,
                                        image: getToolTipImage(r.start)
                                    }))
                                };
                            } else {
                                newTextObject[index] = {...txt, text: value.data};
                            }

                            setTexts(newTextObject);
                        })
                } catch (e) {
                    console.log(e)
                }

            })
    }

    const getTooltipColor = (conf) => {
        switch (true) {
            case conf > 0.8:
                return 'cyan'
            case conf > 0.6:
                return 'orange'
            default:
                return 'pink'
        }
    }


    const getToolTipImage = (offset) => {
        const arr = records.filter((a) => a.type === 'photo').sort((a, b) => (a.path.split("/").at(-1).replace("-info.jpg", "") - b.path.split("/").at(-1).replace("-info.jpg", "")));
        let frame = arr.find((r, i) => {
            return r.path.split("/").at(-1).replace("-info.jpg", "") - arr[0].path.split("/").at(-1).replace("-info.jpg", "") > offset * 1000
        });
        if (!frame) {
            frame = arr.find((r, i) => {
                return r.path.split("/").at(-1).replace("-info.jpg", "") - arr[0].path.split("/").at(-1).replace("-info.jpg", "") > offset * 100
            });
        }
        return frame;
    }

    return (
        <>
            {records && texts && texts.length > 0 ?
                <>
                    <h3>Raw text:</h3>

                    {React.Children.toArray(texts.map((txt) => (
                        <>
                            {txt?.meta &&
                                <>
                                    <Text>
                                        {React.Children.toArray(txt.meta.map((t => (
                                            <Tooltip
                                                title={() => {
                                                    return (
                                                        <>
                                                            {t.image &&
                                                                <img alt={''} width={200} src={"/" + t.image?.path}/>
                                                            }
                                                            {
                                                                t.image && Object.keys(t.image.details[0] || {}).map((k) => (
                                                                    <>
                                                                        <b>{k}:</b>
                                                                        {parseFloat(t?.image.details[0][k] * 100).toFixed(3)}%<br/>
                                                                    </>
                                                                ))
                                                            }
                                                            <Divider/>
                                                            <b>Confidence:</b> {t.conf}
                                                            <br/>
                                                            {t.image && <>
                                                                <b>Time:</b> {format(new Date(t.image.date), 'HH:mm:ss')}</>}
                                                        </>
                                                    )
                                                }}
                                                color={getTooltipColor(t.conf)}>
                                                {t.word} {" "}
                                            </Tooltip>

                                        ))))}
                                    </Text>
                                </>
                            }
                            {!txt?.meta && txt?.text &&
                                <>
                                    <Text>
                                        {txt.text}
                                    </Text>
                                </>
                            }
                            <br/>
                            <br/>
                            <h3>Emotions:</h3>
                            <TextBarChart data={txt.details}/>

                            <br/>
                            <h3>Personality:</h3>
                            <Row>
                                <Col span={6}>
                                </Col>
                                <Col span={12}>
                                    <TextRadarChart data={txt.prediction}/>
                                </Col>
                                <Col span={5}>
                                </Col>
                            </Row>
                        </>
                    )))}
                </> :
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            }
        </>
    )
}

export default React.memo(TextReport);
