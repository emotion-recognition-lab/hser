import React from 'react'
import {useState} from 'react';
import './../styles/global.less';
import Axios from "axios";
import ProfileCard from "./profile-card";
import {Row, Col, Select, Input} from 'antd';

export default function Profiles() {
    const [profiles, setProfiles] = useState([]);
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState(null);
    const [search, setSearch] = useState('');

    React.useEffect(() => {
        Axios.get(`/api/profiles`)
            .then(res => {
                setProfiles(res.data);
                const cats = {};
                const catsArr = [];
                res.data.map(p => {
                    if (p.category.id && !cats[p.category.id]) {
                        cats[p.category.id] = p.category.title;
                        catsArr.push({
                            id:p.category.id,
                            title:p.category.title
                        })
                    }
                });

                setCategories(catsArr);
            })
    }, []);

    return (
        <>
            <Row
                style={{
                    margin: 20
                }}
                wrap={true}
                align={"middle"}
                justify={'center'}
                gutter={12}
            >
                <Col>
                    Filters:
                </Col>
                <Col>
                    <Input
                        style={{
                            minWidth: 300
                        }}
                        placeholder={'Search By Name'}
                        onChange={(event) => {
                        setSearch(event.target.value);
                    }} />
                </Col>
                <Col>
                    <Select
                        style={{
                            minWidth: 300
                        }}
                        defaultValue={null}
                        onChange={(e)  => {
                            setCategory(e);
                        }}
                        showSearch
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        <Option key={'-'} value={null}>All Categories</Option>
                        {categories.map(c => (<Option key={c.id} value={c.id}>{c.title}</Option>))}
                    </Select>
                </Col>
            </Row>
            <Row
                wrap={true}
                align={"middle"}
                justify="start"
                style={{margin: 48}}
                gutter={[24,24]}>
                {profiles
                    .filter(i => {
                        if (!category) {
                            return true;
                        }
                        return i.category?.id === category;
                    })
                    .filter(i => {
                        if (search && search.length > 0) {
                            return i.name.toLowerCase().indexOf(search.toLowerCase()) > -1
                        }
                        return true;
                    })
                    .map((profile) => (
                    <Col className="gutter-row">
                        <ProfileCard profile={profile}/>
                    </Col>
                ))}
            </Row>
        </>
    )
}
