import React, {useEffect, useState} from 'react';
import {Button, Modal, Form, Input, Select, message, DatePicker} from 'antd';
import {useRouter} from 'next/router'

const fetcher = (url) => fetch(url).then((res) => res.json())
import axios from "axios";

const {Option} = Select;

const ProfileCreateForm = ({visible, onCreate, onCancel}) => {
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState();
    const [showForm, setShowForm] = useState(false);
    const [nationalId, setNationalId] = useState();

    const router = useRouter();
    const [form] = Form.useForm();
    const [nationalIdForm] = Form.useForm();

    useEffect(() => {

        axios.get('/api/interview/categories')
            .then((resp) => {
                setCategories(resp.data.categories);
            }).catch((e) => {
            console.log(e);
            message.error('Cannot fetch data');
        }).finally(() => {
            // setIsLoading(false);
        });
    }, []);

    const checkFirstStep = async () => {

        nationalIdForm
            .validateFields()
            .then(values => {
                axios.get('/api/profile/by_national_id/' + values.national_id)
                    .then((resp) => {
                        if (resp.data.id) {
                            router.push({
                                pathname: '/profile/[id]',
                                query: {
                                    id: resp.data.id,
                                    category: values.category
                                },
                            });
                        } else {
                            setCategory(values.category);
                            setNationalId(values.national_id);
                            setShowForm(true);
                        }
                    }).catch((e) => {
                    console.log(e);
                    message.error('Cannot fetch data');
                });


            });
    }
    const createProfile = () => {
        form
            .validateFields()
            .then((values) => {
                form.resetFields();
                values.national_id = nationalId;
                if (values.birthday) {
                    values.birthday = (new Date(values.birthday)).valueOf()
                }

                axios.put("/api/profile/create", values).then(res => {
                    const {data} = res;
                    router.push({
                        pathname: '/profile/[id]',
                        query: {
                            id: data.id,
                            category: category
                        },
                    });
                }).catch(e => {
                    console.log(e)
                })

            })
            .catch((info) => {
                console.log('Validate Failed:', info);
            });
    }

    const onOkay = () => {
        if (!showForm) {
            checkFirstStep();
        } else {
            createProfile();
        }
    }

    return (
        <Modal
            visible={visible}
            title="Start New Interview"
            okText={!nationalId ? "Next" : "Create"}
            cancelText="Cancel"
            onCancel={() => {
                nationalIdForm.resetFields();
                // form.resetFields();
                setShowForm(false);
                onCancel();
            }}
            onOk={onOkay}
        >
            {!showForm &&
                <Form
                    form={nationalIdForm}
                    layout="vertical"
                    name="form_in_modal"
                >
                    <Form.Item
                        name="category"
                        label="Category"
                        rules={[
                            {
                                required: true,
                                message: 'Select the interview\'s category',
                            },
                        ]}
                    >
                        <Select
                            showSearch
                            filterOption={(input, option) =>
                                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {categories.map(c => (<Option value={c.id}>{c.title}</Option>))}
                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="national_id"
                        label="National Id"
                        rules={[
                            {
                                required: true,
                                message: 'Please input the National Id of Profile!',
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                </Form>
            }
            {showForm &&
                <Form
                    form={form}
                    layout="vertical"
                    name="form_in_modal"
                >
                    <Form.Item
                        name="name"
                        label="First Name"
                        rules={[
                            {
                                required: true,
                                message: 'Please input the last name of Profile!',
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="family"
                        label="Last Name"
                        rules={[
                            {
                                required: false,
                                message: 'Please input the last name of Profile!',
                            },
                        ]}
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name="gender"
                        label="Gender"
                        rules={[
                            {
                                required: false,
                            },
                        ]}
                    >
                        <Select>
                            <option value={0}>Male</option>
                            <option value={1}>Female</option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="birthday"
                        label="Birthday"
                        rules={[
                            {
                                required: false,
                            },
                        ]}
                    >
                        <DatePicker/>
                    </Form.Item>

                </Form>
            }
        </Modal>
    );
};

export default function ProfilePage() {
    const [visible, setVisible] = useState(false);

    const onCreate = React.useCallback((values) => {


    });

    return (
        <>
            <Button
                type="dashed"
                round
                onClick={() => {
                    setVisible(true);
                }}
            >
                New Profile
            </Button>
            <ProfileCreateForm
                visible={visible}
                onCreate={onCreate}
                onCancel={() => {
                    setVisible(false);
                }}
            />
        </>
    );
};
