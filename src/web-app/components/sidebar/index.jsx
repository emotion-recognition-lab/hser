import React, {useState} from 'react';
import {Col, Divider, Layout, Row, Space} from "antd";
import "./style.less";
import { Menu } from 'antd';
import { WarningOutlined, UsergroupAddOutlined, FlagOutlined, AreaChartOutlined } from '@ant-design/icons';
const rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];
import Link from 'next/link'
import {useUser} from "../../lib/useUser";

const {Sider} = Layout;

export default function Sidebar() {
    const [openKeys, setOpenKeys] = React.useState([]);
    const [selectedKeys, setSelectedKeys] = React.useState([]);
    const {user} = useUser()
    const [isAdmin, setIsAdmin] = useState(false);

    React.useEffect(() => {
        if (user && user.permissions && user.permissions.includes('admin')) {
            setIsAdmin(true);
        } else {
            setIsAdmin(false);
        }
    }, [user])

    React.useEffect(() => {
        const path = location.pathname.split("/");
        if (path[1] === '' ) {
            setSelectedKeys('dashboard');
        } else {
            path.push(location.pathname);
            setSelectedKeys(path)
        }
    }, []);

    const onOpenChange = keys => {
        const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };



    return (
        <Sider className={'sidebar-wrapper'}>
            <Row  className={'sidebar'}>

            <Row align="top" className={"menus"}>
                <div className="logo">
                    <img src={'/icon.svg'} alt={'Human Emotion Recognition Lab'}/>
                    HESR Lab
                </div>

                <Menu mode="inline" openKeys={openKeys} onOpenChange={onOpenChange} selectedKeys={selectedKeys} style={{width: 203}}>
                    <Menu.Item key="dashboard" icon={<AreaChartOutlined />} >
                        <Link href={'/'}>
                            Dashboard
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sysinfo" icon={<AreaChartOutlined />}>
                        <Link href={'/sysinfo'}>
                            System Monitoring
                        </Link>
                    </Menu.Item>

                    {isAdmin &&
                    <>
                        <Menu.Item key="/interview/categories" icon={<UsergroupAddOutlined />}>
                            <Link href={'/interview/categories'}>
                                Interview Categories
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="/user/list" icon={<UsergroupAddOutlined />}>
                            <Link href={'/user/list'}>
                                User Management
                            </Link>
                        </Menu.Item>
                    </>

                    }
                    {isAdmin &&
                        <Menu mode="inline" openKeys={openKeys} onOpenChange={onOpenChange} selectedKeys={selectedKeys} style={{width: 203}}>
                            <Menu.Item key="sub1" icon={<WarningOutlined />}>
                                <Link href={'/configs/multi-factor-ratio'}>
                                    Configuration
                                </Link>
                            </Menu.Item>
                        </Menu>

                        }

                </Menu>
            </Row>
            <Row align="bottom" className={'side-footer'}>
                <Menu mode="inline" openKeys={openKeys} onOpenChange={onOpenChange} selectedKeys={selectedKeys} style={{width: 203}}>
                    <Menu.Item key="help" icon={<WarningOutlined />}>
                        <Link href={'/help'}>
                            Help
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="about-us" icon={<FlagOutlined />}>
                        <Link href={'/about-us'}>
                            About Us
                        </Link>
                    </Menu.Item>
                </Menu>
                <Divider/>
                <Row >
                    <Col align={'center'}>

                        <a target={'_blank'} href={'http://liau.ac.ir'}><img src={'/aut-logo.png'} width={'80%'}/></a>
                        <small>Version: 0.10.12</small>

                    </Col>
                </Row>
            </Row>
            </Row>
        </Sider>
    )
}
