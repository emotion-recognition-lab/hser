import React, {useState, useCallback} from 'react';
import {Alert, Button, Col, Divider, Popconfirm, Row, Space, Spin, Tabs, Tooltip} from "antd";
import Gallery from "react-photo-gallery";
import Carousel, {Modal, ModalGateway} from "react-images";
import {PrinterFilled} from '@ant-design/icons';
import Axios from "axios";
import Socket from "../../lib/useSocket";
import VoiceRadarChart from "../charts/voice-radar";
import Player from "../player";
import html2pdf  from 'html2pdf.js';

import './style.less'
import Report from "../report";
import TextReport from "../text-report";
import VoicePieChart from "../charts/voice-pie";

const {TabPane} = Tabs;
export default function StreamDetails({profileId, streamId}) {
    const [snapshots, setSnapshots] = useState([]);
    const [stream, setStream] = useState();
    const [texts, setTexts] = useState([]);
    const [voices, setVoices] = useState([]);
    const [videos, setVideos] = useState([]);
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);
    const [loading, setLoading] = useState(true);
    const [isPrinting, setIsPrinting] = useState(false);
    const [isWithSign, setIsWithSign] = useState(false);
    const [activeTab, setActiveTab] = useState('report');

    const openLightbox = useCallback((event, {photo, index}) => {
        setCurrentImage(index);
        setViewerIsOpen(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
    };


    const exportPDF =(withSign)  => {
        setIsPrinting(true);
        setIsWithSign(withSign);
        setTimeout(() => {
        const opt = {
            margin:       1,
            filename:     `report.pdf`,
            image:        { type: 'jpeg', quality: 0.98 },
            html2canvas:  { scale: 1 },
            jsPDF:        { unit: 'cm', format: 'A4', orientation: 'portrait' }
        };
        html2pdf().set(opt).from(document.getElementById('report-container')).save()
            .then(()=>{
                setTimeout(() =>  {
                setIsPrinting(false);
                }, 500)
            });
        }, 1000)
    };

    React.useEffect(() => {
        setVideos([]);
        setLoading(true)
        Axios.get(`/api/profile/${profileId}/stream/${streamId}`)
            .then((res) => {
                return res.data;
            })
            .then((stream) => {
                setStream(stream);
                Axios.get(`/api/profile/${profileId}/records/${streamId}`)
                    .then(res => {
                        setSnapshots(res.data.filter(r => r.type === 'photo'));
                        if (res.data.filter(r => r.type === 'photo').length > 0) {
                            setVideos([stream]);
                        }

                        setVoices(res.data.filter(r => r.type === 'voice'));
                        setLoading(false);
                        setActiveTab('report');
                        const textItems = res.data.filter(r => r.type === 'text').map(txt => ({
                            ...txt,
                            prediction: JSON.parse(txt.prediction),
                            details: typeof txt.details === "string" ? JSON.parse(txt.details) : {}
                        }));
                        setTexts(textItems);
                        setTimeout(() => {
                            loadTexts(textItems);
                        }, 1000)
                    });
            });

        Socket.getInstance();
    }, [streamId]);


    const loadTexts = (textsObj) => {
        texts
            .forEach((txt, index) => {
                Axios.get(`/${txt.path}`)
                    .then((value) => {
                        const newTextObject = [...texts];
                        newTextObject[index] = {...txt, text: value.data};
                        setTexts(newTextObject);
                    })
            })
    }

    if (!stream) {
        return <Spin/>
    }

    return (
        <>
            {!loading && activeTab === 'report' &&
                <div className={'report-print-btn'}>
                    <Popconfirm
                        placement="bottomRight"
                        title={'Do you want to print with approval signature?'}
                        onConfirm={() => exportPDF(true)}
                        okText="Yes"
                        onCancel={() => exportPDF(false)}
                        cancelText="No"
                    >
                        <Button loading={isPrinting} icon={ <PrinterFilled />}>Print</Button>
                    </Popconfirm>
                </div>
            }
            {!loading &&
            <Tabs defaultActiveKey={activeTab} activeKey={activeTab} onChange={t => setActiveTab(t)}>
                <TabPane tab="Report" key="report">
                    <div className={isPrinting ? isWithSign ? 'print-mode sign' : 'print-mode without-sign' : ''}>
                    <Report
                        showSign={isWithSign}
                        profileId={profileId}
                        voiceData={voices}
                        videoData={videos}
                        textData={texts}
                        stream={stream}
                    />
                    </div>
                </TabPane>
                {videos.length > 0 &&
                <TabPane tab="Video" key="video">
                    {videos.map(video => (
                        <Player url={`/${video.path}`}/>
                    ))
                    }
                </TabPane>
                }
                {snapshots.length > 0 &&
                <TabPane tab="Snapshots" key="snapshots">

                    <Gallery onClick={openLightbox} photos={snapshots.map(snapshot => ({
                        src: `/${snapshot.path}`,
                        width: 4,
                        height: 3
                    }))}/>
                    <ModalGateway>
                        {viewerIsOpen ? (
                            <Modal onClose={closeLightbox}>
                                <Carousel
                                    currentIndex={currentImage}
                                    views={snapshots.map(x => ({
                                        ...x,
                                        source: `/${x.path}`,
                                        caption: x.date
                                    }))}
                                />
                            </Modal>
                        ) : null}
                    </ModalGateway>
                </TabPane>
                }
                {voices.length > 0 &&
                <TabPane tab="Voice" key="voice">
                    {React.Children.toArray(voices.map((voice) => (
                        <>
                            <Row>
                                <Col span={24}>
                                    <Player url={`/${voice.path}`} emotions={voice.details.row}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={12}>
                                    <VoicePieChart data={voice.prediction} details={voice.details}/>
                                </Col>
                            </Row>
                        </>
                    )))}

                </TabPane>
                }
                {texts.length > 0 &&
                <TabPane tab="Text" key="text">
                    <TextReport records={snapshots} textsItems={texts}/>
                </TabPane>
                }

            </Tabs>
            }
        </>
    )
}
