import {ResponsiveRadar} from '@nivo/radar'
import {useEffect, useState} from "react";

export default function TextRadarChart(props) {
    const [data, setData] = useState([]);

    useEffect(() => {
        const traitsArray = []
        const traits = Object.keys(props.data);
        traits.forEach((trait) => {
            traitsArray.push({
                trait,
                prediction:  (props.data[trait] * 100).toFixed(2)
            })
        });
        setData(traitsArray);

    }, [props.data]);

    return (
        <div style={{height: 400, marginRight: '20%'}}>
            <ResponsiveRadar
                data={data}
                keys={['prediction']}
                indexBy="trait"
                maxValue="auto"
                margin={{top: 70, right: 80, bottom: 40, left: 80}}
                curve="linearClosed"
                borderWidth={2}
                borderColor={{from: 'color'}}
                gridLevels={5}
                gridShape="linear"
                gridLabelOffset={36}
                enableDots={true}
                dotSize={10}
                dotColor={{theme: 'background'}}
                dotBorderWidth={2}
                dotBorderColor={{from: 'color'}}
                enableDotLabel={true}
                dotLabel="value"
                dotLabelYOffset={-12}
                colors={{scheme: 'nivo'}}
                fillOpacity={0.25}
                blendMode="multiply"
                animate={true}
                motionConfig="wobbly"
                isInteractive={true}
            />
        </div>
    )
}
