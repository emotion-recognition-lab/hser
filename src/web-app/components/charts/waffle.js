import React, {useState} from "react";
import Axios from "axios";
import { ResponsiveWaffle } from '@nivo/waffle'

const Waffle = ({ totol, data /* see data tab */ }) => {
    return (
        <div style={{height: 150, minWidth: 300, width : '100%'}}>

        <ResponsiveWaffle
            data={data}
            total={100}
            rows={1}
            columns={30}
            margin={{ top: 10, right: 10, bottom: 10, left: 10 }}
            colors={{ scheme: 'set1' }}
            borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.3 ] ] }}
            animate={true}
            motionStiffness={90}
            motionDamping={11}
            legends={[
                {
                    anchor: 'top-left',
                    direction: 'row',
                    justify: false,
                    translateX: 0,
                    translateY: 0,
                    itemsSpacing: 4,
                    itemWidth: 150,
                    itemHeight: 20,
                    itemDirection: 'left-to-right',
                    itemOpacity: 1,
                    itemTextColor: '#777',
                    symbolSize: 20,
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000',
                                itemBackground: '#f7fafb'
                            }
                        }
                    ]
                }
            ]}
        />
        </div>
    )
}

export default Waffle;
