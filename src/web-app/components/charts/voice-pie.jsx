import React, {useEffect, useState} from "react";
import { Bar } from 'react-chartjs-2';
import { Tableau20 } from 'chartjs-plugin-colorschemes/src/colorschemes/colorschemes.tableau';
import {Alert} from "antd";

export default function TextBarChart(props) {
    const [labels, setLabels] = useState([props.data.split(',')]);
    const [values, setValues] = useState([props.details.mapping]);

    if (!props.data) {
        return  (
            <Alert
                message="No Data"
                description="There is no enough data to display."
                type="warning"
            />
        );
    }

    useEffect(()=> {

        //     const traitsArray = [];
        //     const percentages = props.data.split(',').map(i => parseInt(i, 10))
        //     percentages.forEach((percent, index) => {
        //         traitsArray.push({
        //             trait: props.details.mapping[index.toString()],
        //             prediction:  percent
        //         })
        //     });
        //     setData(traitsArray);
        //
        // const data = props.data.emotions.map((emotion, i) => ({ emotion, value :props.data.predictions[i]}))
        //     .sort((a, b) => b.value - a.value)

        setValues(props.data.split(','));
        setLabels(Object.values(props.details.mapping));
    }, [props.data])

    const options = {

        indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
            bar: {
                borderWidth: 2,
            },
        },
        onAnimationComplete: function()
        {
            this.showTooltip(this.datasets[0].bars, true);
        },
        responsive: false,
        plugins: {
            legend: {
                display: false,
                position: 'right',
            },
            title: {
                display: false,
                text: 'Chart.js Horizontal Bar Chart',
            },
        },
    };

    return (
        <div style={{padding: 50}}>
            <Bar
                height={300}
                width={500}
                data={{
                    labels: labels,
                    datasets: [
                        {
                            label: '%',
                            data: values,
                            borderWidth: 1,
                            backgroundColor: Tableau20
                        },
                    ],
                }} options={options} />
        </div>
    )
}
