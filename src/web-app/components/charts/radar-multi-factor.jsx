import {ResponsiveRadar} from '@nivo/radar'
import {useEffect, useState} from "react";
import {isNumber} from "lodash/lang";

export default function RadarMultiFactor(props) {
    const [data, setData] = useState([]);

    useEffect(() => {
        const labels = Object.keys(props.items);
        const tArray = []
        try {
            props.emotions.forEach((emotion) => {
                const emotionData = {};
                let value = [];
                labels.forEach((label) => {
                    const itemValue = props.items[label][emotion];
                    if (isNumber(itemValue)) {
                        emotionData[label] = parseFloat(itemValue).toFixed(2);
                        value.push(props.items[label][emotion] * props.ratio[emotion][label]);
                    }
                })

                const resultant = getAverage(value);
                const itemObject = {
                    emotion: emotion,
                    ...emotionData,
                }

                if (resultant != 'NaN') {
                    itemObject.resultant = resultant;
                }

                tArray.push(itemObject);
            });

        } catch (error) {
        }
        setData(tArray);

    }, [props.items, props.emotions]);

    const getAverage = (arr) => {
        let sum = 0;
        let count = 0;
        arr.forEach((a) => {
            if (!isNaN(a)) {
                sum = sum + a;
                count++;
            }
        });
        return (sum / count).toFixed(2);
    }

    return (
        <div style={{height: 400}}>
            {data.length > 0 && (
                <ResponsiveRadar
                    keys={[...Object.keys(data[0]).filter((a) => a !== 'emotion')]}
                    blendMode="darken"
                    data={data}
                    indexBy="emotion"
                    maxValue="auto"
                    margin={{top: 70, right: 80, bottom: 40, left: 80}}
                    curve="cardinalClosed"
                    borderWidth={2}
                    borderColor={{from: 'color'}}
                    gridLevels={5}
                    gridShape="linear"
                    gridLabelOffset={14}
                    enableDots={true}
                    dotSize={10}
                    dotColor={{theme: 'background'}}
                    dotBorderWidth={2}
                    dotBorderColor={{from: 'color'}}
                    enableDotLabel={false}
                    dotLabel="value"
                    dotLabelYOffset={-12}
                    colors={{scheme: 'nivo'}}
                    fillOpacity={0.25}
                    animate={true}
                    motionConfig="wobbly"
                    isInteractive={true}

                />
            )}
        </div>
    )
}
