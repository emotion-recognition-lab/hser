import {ResponsiveRadar} from '@nivo/radar'
import {useEffect, useState} from "react";

export default function VoiceRadarChart(props) {
    const [data, setData] = useState([]);

    useEffect(() => {
        try {

        const traitsArray = [];
        const percentages = props.data.split(',').map(i => parseInt(i, 10))
        percentages.forEach((percent, index) => {
            traitsArray.push({
                trait: props.details.mapping[index.toString()],
                prediction:  percent
            })
        });
        setData(traitsArray);
        } catch (e) {}

    }, [props.data, props.details]);

    return (
        <div style={{height: 400, marginRight: '20%'}}>
            <ResponsiveRadar
                data={data}
                keys={['prediction']}
                indexBy="trait"
                maxValue="auto"
                curve="linearClosed"
                borderWidth={2}
                borderColor={{from: 'color'}}
                gridLevels={5}
                gridShape="linear"
                gridLabelOffset={36}
                enableDots={true}
                dotSize={10}
                dotColor={{theme: 'background'}}
                dotBorderWidth={2}
                dotBorderColor={{from: 'color'}}
                enableDotLabel={true}
                dotLabel="value"
                dotLabelYOffset={-12}
                colors={{scheme: 'nivo'}}
                fillOpacity={0.25}
                blendMode="multiply"
                animate={true}
                motionConfig="wobbly"
                isInteractive={true}
            />
        </div>
    )
}
