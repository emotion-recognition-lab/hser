import {ResponsiveAreaBump} from '@nivo/bump'
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import Socket from "../../lib/useSocket";

const ChartBar = ({profileId, streamId}) => {
    const [data, setData] = useState([]);
    let dataRef = useRef();
    dataRef.current = data;

    const pushRecord = (record, updateState = true) => {
        const records = [...dataRef.current];
        if (record.details[0]) {
            Object.keys(record.details[0]).forEach((emotion, emotionIndex) => {
                const emotionRecordsIndexIndex = records.findIndex(e => e.id === emotion)

                if (emotionRecordsIndexIndex === -1) {
                    records.push({
                        id: emotion,
                        data: [{
                            x: records.date,
                            y: record.details[0][emotion] * 1000
                        }]
                    });
                } else {
                    records[emotionRecordsIndexIndex].data.push({
                            x: record.date,
                            y: record.details[0][emotion] * 1000
                        }
                    );
                }
            });
        }
        if (updateState) {
            setData(records);
        }
        return records;
    }

    const dataTransformer = (records) => {
        let newRecords = [];
        records.forEach((record) => {
            if (record.type === "photo") {
                newRecords = pushRecord(record);
            }
        });
        return newRecords;
    }


    useEffect(() => {
        if (!streamId || !profileId) return;

        const eventSub = (...args) => {
            const array = new Array(args);
            const info = array[0][0];
            info.date = Date.now();
            pushRecord(info);
        };

        Axios.get(`/api/profile/${profileId}/records/${streamId}`)
            .then(res => {
                setData(dataTransformer(res.data));
                Socket.getInstance().on('SNAPSHOT_PROCESSED', eventSub);
            });

        return function cleanUp() {
            Socket.getInstance().removeListener('SNAPSHOT_PROCESSED', eventSub);
        }
    }, [streamId])
    if (data.length === 0) return null;

    return (
        <div style={{height: 400, minWidth: 300, width: data[0].data.length * 20}}>
            <ResponsiveAreaBump
                data={data}
                legends={[
                    {
                        anchor: 'bottom-right',
                        direction: 'column',
                        justify: false,
                        translateX: 100,
                        translateY: 0,
                        itemsSpacing: 0,
                        itemDirection: 'left-to-right',
                        itemWidth: 80,
                        itemHeight: 20,
                        itemOpacity: 0.75,
                        symbolSize: 12,
                        symbolShape: 'circle',
                        symbolBorderColor: 'rgba(0, 0, 0, .5)',
                        effects: [
                            {
                                on: 'hover',
                                style: {
                                    itemBackground: 'rgba(0, 0, 0, .03)',
                                    itemOpacity: 1
                                }
                            }
                        ]
                    }
                ]}
                margin={{top: 40, right: 100, bottom: 40, left: 100}}
                spacing={3}
                colors={{scheme: 'nivo'}}
                blendMode="multiply"
                startLabel="id"
                axisBottom={{
                    tickSize: 0,
                    tickPadding: 5,
                    tickRotation: -90,
                    legend: '',
                    legendPosition: 'middle',
                    legendOffset: 60
                }}
                animate={false}

            />
        </div>
    )
};
export default ChartBar;
