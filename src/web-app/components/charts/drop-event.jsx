import * as d3 from 'd3';
import eventDrops from 'event-drops';
import React, {useEffect, useState} from "react";
import Axios from "axios";
import "./style.less"

export const DropEvent = ({profileId, streamId, textData}) => {
    const [data, setData] = useState([]);
    const [tooltip, setTooltip] = useState();
    const dataTransformer = (rawData => {
        const data = [
            {
                name: "Angry",
                data: []
            },
            {
                name: "Disgust",
                data: []
            },
            {
                name: "Fear",
                data: []
            },
            {
                name: "Happy",
                data: []
            },
            {
                name: "Neutral",
                data: []
            },
            {
                name: "Sad",
                data: []

            },
            {
                name: "Surprise",
                data: []
            }
        ];

        rawData.filter(data => data.type === 'photo')

            .forEach(point => {
                if (point.details[0]) {
                    Object.keys(point.details[0]).forEach((e, i) => {
                        data[i].data.push({
                            date: point.date,
                            value: point.details[0][e],
                            info: point
                        })
                    })
                }
            });
        return data;
    });


    useEffect(() => {
        if (!streamId || !profileId) return;

        Axios.get(`/api/profile/${profileId}/records/${streamId}`)
            .then(res => {
                setData(dataTransformer(res.data));
            });
    }, [streamId]);

    useEffect(() => {
        const tooltipObj = d3
            .select('body')
            .append('div')
            .classed('tooltip', true)
            .style('opacity', 1)
            .style('pointer-events', 'auto');
        setTooltip(tooltipObj);
        return () => {
            tooltipObj.remove();
        }
    }, [])

    const getText = (start) => {

        const offset = 100;
        if (!textData) return null;

        const texts = [];
        let goForward = true;
        let index = 0;
        while (goForward) {
            if(textData[index]) {
                texts.push(textData[index].start + ' => ' + textData[index].end + " => " + start + ' <br/>');

            }
            if (
                textData[index] &&
                textData[index].start > start - offset &&
                textData[index].end < start + offset
            ) {
            } else if ( !textData[index] || textData[index].end > start + offset || index + 1 === textData.length){
                goForward = false;
            }
            index ++;
        }
        return texts;
    }

    useEffect(() => {
        if (data.length === 0) return

        const startDate = data[0].data[0].date;
        const endDate = data[0].data[data[0].data.length -1].date;

        const chart = eventDrops({
            d3,

            range: {
                start: new Date(startDate + (startDate - endDate) * 0.1) ,
                end: new Date(endDate - (startDate - endDate) * 0.1) ,
            },
            label: {
                text: row => row.name,
            },
            axis: {
                formats: {
                    // this is the default value
                    milliseconds: '%L',
                    seconds: '%M:%S',
                    minutes: '%I:%M',
                    hours: '%I %p',
                    days: '%a %d',
                    weeks: '%b %d',
                    months: '%B',
                    year: '%Y',
                },
            },
            drop: {
                date: d => new Date(d.date),

                radius: (d, index) => Math.abs(Math.log2(d.value * 100)) ,
                onMouseOver: d => {
                    tooltip
                        .transition()
                        .duration(200)
                        .style('opacity', 1)
                        .style('pointer-events', 'auto');
                    tooltip
                        .html(
                            `
                                <div class="commit">
                                    <img src='/${d.info.path}'/>
                                    <div>
                                    ${
                                        Object.keys(d.info.details[0]).map((k) => (
                                            `<b>${k}:</b>${parseFloat(d.info.details[0][k] * 100).toFixed(3) }%<br>`
                                        )).join('')
                                    }
                                    </div>
                                </div>
                            `
                        )
                    .style('left', `${d3.event.pageX - 30}px`)
                    .style('top', `${d3.event.pageY + 20}px`);
                },
                onMouseOut: () => {
                    tooltip
                        .transition()
                        .duration(500)
                        .style('opacity', 0)
                        .style('pointer-events', 'none');
                },
            },
        });

        d3
            .select('#eventdrops')
            .data([data])
            .call(chart);
    }, [data, textData]);

    return (
        <div style={{width:'80%',  marginRight: '10%'}}>
            <div id={'eventdrops'}/>
        </div>
    )
}

