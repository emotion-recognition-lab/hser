import {useEffect, useState} from "react";
import RadarMultiFactor from "../charts/radar-multi-factor";
import {isNumber} from "lodash/lang";
import Axios from "axios";

const emotions = ['Angry', 'Disgust', 'Fear', 'Happy', 'Neutral', 'Sad', 'Surprise'];
const textEmotions = ['admiration', 'amusement', 'anger', 'annoyance', 'approval', 'caring', 'confusion', 'curiosity', 'desire', 'disappointment', 'disapproval', 'disgust', 'embarrassment', 'excitement', 'fear', 'gratitude', 'grief', 'joy', 'love', 'nervousness', 'optimism', 'pride', 'realization', 'relief', 'remorse', 'sadness', 'surprise', 'neutral'];

const textEmotionsCategoriesMapping = {
    anger: 'Angry',
    nervousness: 'Angry',

    disappointment: 'Disgust',
    disapproval: 'Disgust',
    disgust: 'Disgust',

    embarrassment: 'Fear',
    remorse: 'Fear',
    fear: 'Fear',

    admiration: 'Happy',
    optimism: 'Happy',
    pride: 'Happy',
    realization: 'Happy',
    amusement: 'Happy',
    approval: 'Happy',
    caring: 'Happy',
    desire: 'Happy',
    excitement: 'Happy',
    joy: 'Happy',

    neutral: 'Neutral',
    relief: 'Neutral',
    gratitude: 'Neutral',
    love: 'Neutral',

    annoyance: 'Sad',
    grief: 'Sad',

    confusion: 'Surprise',
    curiosity: 'Surprise',
    sadness: 'Surprise',
    surprise: 'Surprise',

}
export default function MultiFactorResult({records}) {
    const [photoAverages, setPhotoAverages] = useState({});
    const [voiceAverages, setVoiceAverages] = useState({});
    const [textAverages, setTextAverages] = useState({});
    const [ratio, setRatio] = useState(null);

    useEffect(() => {
        Axios.get('/api/administration/configs/multi-factor-ratio')
            .then((response) => {
                setRatio(response.data.value)
            });
    }, []);

    useEffect(() => {
        if (records && ratio) {
            let voiceData = {};
            let photoData = {
                Angry: [],
                Disgust: [],
                Fear: [],
                Happy: [],
                Neutral: [],
                Sad: [],
                Surprise: [],
            };
            let photoAverageData = {};
            let textAverage = {};

            records.forEach((r) => {
                if (r.type === 'voice') {
                    const values = r.prediction.split(',');
                    values.forEach((m, i) => {
                        voiceData[r.details.mapping[i]] = isNaN(parseFloat(m, 10)) ? 0 : parseFloat(m, 10);
                    });
                } else if (r.type === 'photo') {
                    if (r.details.length > 0) {
                        Object.keys(photoData).forEach((label) => {
                            photoData[label].push(isNaN(r.details[0][label]) ? 0 : r.details[0][label]);
                        })
                    }
                } else if (r.type === 'text') {
                    const textData = JSON.parse(r.details);
                    const sum = textData.predictions.reduce((a, b) => a + b, 0);

                    textData.emotions.forEach((e, i) => {
                        const value = textData.predictions[i] || 0;
                        const emotion = textEmotionsCategoriesMapping[e];

                        if (textAverage[emotion]) {
                            textAverage[emotion] += ((value / sum) * 100);
                        } else {
                            textAverage[emotion] = ((value / sum) * 100);
                        }
                    })
                }

            });
            Object.keys(photoData).forEach((label) => {
                photoAverageData[label] = 100 * photoData[label].reduce((a, b) => a + b, 0) / photoData[label].length;
                photoAverageData[label] = isNaN(photoAverageData[label]) ? '-' : photoAverageData[label]
            });
            setVoiceAverages(voiceData);
            setPhotoAverages(photoAverageData);
            setTextAverages(textAverage);

        }
    }, [ratio, records]);

    const checkNumber = (input) => {
        const float = parseFloat(input, 10);
        if (!isNumber(float) || isNaN(float)) {
            return '-'
        }
        return input
    }

    if (!records || records.length === 0 || !ratio) {
        return null;
    }

    return (
        <>
            <div className={'ant-table'}>
                <h3>Video-Voice</h3>
                <div className="multi-factor-result">
                    <table>
                        <thead>
                        <tr>
                            <th>Emotion</th>
                            <th>Video</th>
                            <th>Voice</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {emotions.map((label, index) => (
                            <tr>
                                <td>{label}</td>
                                <td>{checkNumber(parseFloat(photoAverages[label]).toFixed(2))}</td>
                                <td>{checkNumber(voiceAverages[label])}</td>

                                <td>{checkNumber(parseFloat(((photoAverages[label] * ratio[label]['video']) + (voiceAverages[label] * ratio[label]['voice'])) / 2).toFixed(2))}</td>
                                {index === 0 &&
                                    <td rowSpan={emotions.length} style={{width: 500}}>
                                        <RadarMultiFactor
                                            emotions={emotions}
                                            ratio={ratio}
                                            items={{
                                                video: photoAverages,
                                                voice: voiceAverages,
                                            }}/>
                                    </td>
                                }
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className={'ant-table'}>
                <h3>Text-Voice</h3>
                <div className="multi-factor-result">
                    <table>
                        <thead>
                        <tr>
                            <th>Emotion</th>
                            <th>Text</th>
                            <th>Voice</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {emotions.map((label, index) => (
                            <tr>
                                <td>{label}</td>
                                <td>{checkNumber(parseFloat(textAverages[label]).toFixed(2))}</td>
                                <td>{checkNumber(voiceAverages[label])}</td>
                                <td>{checkNumber(parseFloat(((textAverages[label] * ratio[label]['text']) + (voiceAverages[label]) * ratio[label]['voice']) / 2).toFixed(2))}</td>
                                {index === 0 &&
                                    <td rowSpan={emotions.length} style={{width: 500}}>
                                        <RadarMultiFactor
                                            emotions={emotions}
                                            ratio={ratio}
                                            items={{
                                                text: textAverages,
                                                voice: voiceAverages,
                                            }}/>
                                    </td>
                                }
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className={'ant-table'}>
                <h3>Text-Video</h3>
                <div className="multi-factor-result">
                    <table>
                        <thead>
                        <tr>
                            <th>Emotion</th>
                            <th>Text</th>
                            <th>Video</th>
                            <th>Value</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {emotions.map((label, index) => (
                            <tr>
                                <td>{label}</td>
                                <td>{checkNumber(parseFloat(textAverages[label]).toFixed(2))}</td>
                                <td>{checkNumber(parseFloat(photoAverages[label]).toFixed(2))}</td>
                                <td>{checkNumber(parseFloat(((textAverages[label] * ratio[label]['text']) + (photoAverages[label] * ratio[label]['video'])) / 2).toFixed(2))}</td>
                                {index === 0 &&
                                    <td rowSpan={emotions.length} style={{width: 500}}>
                                        <RadarMultiFactor
                                            emotions={emotions}
                                            ratio={ratio}
                                            items={{
                                                text: textAverages,
                                                video: photoAverages,
                                            }}/>
                                    </td>
                                }
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className={'ant-table'}>
                <h3>Text-Voice-Video</h3>
                <div className="multi-factor-result" style={{width: 900}}>
                    <table>
                        <thead>
                        <tr>
                            <th>Emotion</th>
                            <th>Value</th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        {emotions.map((label, index) => (
                            <tr>
                                <td>{label}</td>
                                <td>{checkNumber(parseFloat(((photoAverages[label] * ratio[label]['video'])) + (textAverages[label] * ratio[label]['text'])+ (voiceAverages[label] * ratio[label]['voice']) / 3).toFixed(2))}</td>
                                {index === 0 &&
                                    <td rowspan={emotions.length} style={{width: 500}}>
                                        <RadarMultiFactor
                                            emotions={emotions}
                                            ratio={ratio}
                                            items={{
                                                text: textAverages,
                                                voice: voiceAverages,
                                                video: photoAverages,
                                            }}/>
                                    </td>
                                }
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}