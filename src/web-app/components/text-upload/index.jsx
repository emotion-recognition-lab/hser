import {Button, Card, Input, notification, Result, Space} from "antd";
import {FileTextTwoTone} from "@ant-design/icons";
import Modal from "antd/lib/modal/Modal";
import {useEffect, useState} from "react";
import Socket from "../../lib/useSocket";
import { SmileOutlined, SyncOutlined } from '@ant-design/icons';
import {useRouter} from 'next/router'

const {TextArea} = Input;

const TextUpload = function (props) {
    const [stream, setStream] = useState(props.streamId || Date.now());
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [text, setText] = useState('');
    const [isProcessing, SetIsProcessing] = useState(false);
    const [isProcessed, SetIsProcessed] = useState(false);
    const router = useRouter();

    useEffect(() => {
        const listener =  (data) => {
            SetIsProcessed(true);
        };
        Socket.getInstance().on('TEXT_PROCESSED', listener);
        return () => {
            Socket.getInstance().off('TEXT_PROCESSED', listener);
        }
    }, [stream, props.profileId]);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        if (text.trim().length === 0 ) {
            notification.error({
                message: 'The text field is empty'
            })
            return;
        }
        SetIsProcessing(true);
        Socket.getInstance().emit('UPLOAD_TEXT', {
            streamId: stream,
            category: router.query.category,
            profile: { id: props.profileId },
            text
        });
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    return (
        <>
            <Card>
                <div className={'button-area'} onClick={showModal}>
                    <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                        <div className={'icon'}>
                            <FileTextTwoTone style={{fontSize: 36}}/>
                        </div>
                        <p className={'title-text'}>Text Analyzer</p>
                        <p className={'hint-text'}>Import the Text</p>
                    </Space>
                </div>
            </Card>
            <Modal title="Text Analyzer"
                   visible={isModalVisible}
                   onOk={handleOk}
                   onCancel={handleCancel}
                   closable={false}
                   cancelButtonProps={{disabled: isProcessing || isProcessed}}
                   okButtonProps={{disabled: isProcessing || isProcessed}}
                   destroyOnClose
                   maskClosable={false}
                   width={'90vw'}
                   confirmLoading={isProcessing}
            >
                {isProcessing &&
                <Result
                    icon={isProcessed ? <SmileOutlined /> : <SyncOutlined spin /> }
                    title={isProcessed ? "Great, we have done all the operations!" : "We are trying to process the text."}
                    extra={isProcessed && <Button type={'primary'} onClick={() => { props.onDone(stream);}}>Go to Result</Button>}
                />}
                {!isProcessing &&
                <TextArea rows={10} placeholder={'Input the text'}
                          onChange={(e) => {
                              setText(e.target.value)
                          }}/>
                }
            </Modal>
        </>
    )
}

export default TextUpload;
