import {Button, Card, Col, Input, notification, Result, Row, Space} from "antd";
import {VideoCameraTwoTone} from "@ant-design/icons";
import Modal from "antd/lib/modal/Modal";
import {useEffect, useState} from "react";
import Socket from "../../lib/useSocket";
import {SmileOutlined, SyncOutlined} from '@ant-design/icons';
import Webcam from "react-webcam";
import ChartBar from "../charts/bar";
import Axios from "axios";
import {Radio} from 'antd';
import {useRouter} from "next/router";

const LiveInterview = function (props) {
    const [stream, setStream] = useState(props.streamId || Date.now());
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isProcessing, SetIsProcessing] = useState(false);
    const [isProcessed, SetIsProcessed] = useState(false);
    const [isLiveView, setIsLiveView] = useState(true);
    const [latestSnapShot, setLatestSnapShot] = useState(null);
    const router = useRouter();

    const webcamRef = React.useRef(null);
    const mediaRecorderRef = React.useRef(null);
    const [capturing, setCapturing] = React.useState(null);
    const [captureInterval, setCaptureInterval] = React.useState(null);

    const videoConstraints = {
        width: 720,
        height: 480,
        facingMode: "user"
    };


    const handleStartCaptureClick = () => {

        if (capturing) {
            clearInterval(captureInterval);
            setCaptureInterval(null);
            setCapturing(false);
            Socket.getInstance().emit('CAPTURE_VIDEO_FINISHED', {
                streamId: stream,
                category: router.query.category,
                profile: {id: props.profileId},
            });

            SetIsProcessed(true);
            return;
        }


        mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, {
            mimeType: "video/webm"
        });
        mediaRecorderRef.current.addEventListener(
            "dataavailable",
            handleDataAvailable
        );
        setCapturing(true)

        setTimeout(() => {
            handleStartCaptureSnapshotClick();

            mediaRecorderRef.current.start();
            setCaptureInterval(setInterval(requestDataEventCall, 1000));
        }, 1000)
    };


    const requestDataEventCall = () => {
        mediaRecorderRef.current.requestData();
    };

    const handleDataAvailable = ({data}) => {
        if (data.size > 0) {
            Socket.getInstance().emit('CAPTURE_VIDEO', {
                dataType: 'arraybuffer',
                data: data,
                streamId: stream,
                category: router.query.category,
                profile: {id: props.profileId},
            });
        }
    };


    const appendSnapshot = React.useCallback((newSnapshot) => {
        setLatestSnapShot("/" + newSnapshot);
    }, [])

    const handleStartCaptureSnapshotClick = () => {
        const img = webcamRef.current.getScreenshot();
        Socket.getInstance().emit('SNAPSHOT', {
            dataType: 'base64',
            data: img,
            streamId: stream,
            category: router.query.category,
            profile: {id: props.profileId},
        });
    };

    useEffect(() => {
        setStream(Date.now());
    }, [props.profileId]);

    useEffect(() => {
        const listener = (...args) => {
            const array = new Array(args);
            const info = array[0][0];
            appendSnapshot(info.path);
            if (captureInterval) {
                handleStartCaptureSnapshotClick();
            }
        };
        Socket.getInstance().on('SNAPSHOT_PROCESSED', listener);
        return () => {
            Socket.getInstance().off('SNAPSHOT_PROCESSED', listener);
        }
    }, [props.profileId, captureInterval]);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleViewChange = e => {
        setIsLiveView(e.target.value === 'live')
    };

    return (
        <>
            <Card>
                <div className={'button-area'} onClick={showModal}>
                    <Space align={"center"} size={["large"]} style={{marginBottom: 0}} direction={'vertical'}>
                        <div className={'icon'}>
                            <VideoCameraTwoTone style={{fontSize: 36}}/>
                        </div>
                        <p className={'title-text'}>Live Interviewer</p>
                        <p className={'hint-text'}>Live Analyzer</p>
                    </Space>
                </div>
            </Card>
            <Modal title="Text Analyzer"
                   visible={isModalVisible}
                   closable={false}
                   footer={null}
                   destroyOnClose
                   maskClosable={false}
                   width={'90vw'}
                   confirmLoading={isProcessing}
            >
                {isProcessed &&
                <Result
                    icon={isProcessed ? <SmileOutlined/> : <SyncOutlined spin/>}
                    title={isProcessed ? "Great, we have done all the operations!" : "We are trying to process the text."}
                    extra={isProcessed && <Button type={'primary'} onClick={() => {
                        setIsModalVisible(false);
                        props.onDone(stream);
                    }}>Go to Result</Button>}
                />}
                {!isProcessing &&
                <>
                    <Space direction={'vertical'}>
                        <Space direction={'vertical'}>
                            <div>
                                <div>
                                    <Radio.Group value={isLiveView ? 'live' : 'Processing'} onChange={handleViewChange}>
                                        <Radio.Button value={'live'}>Live View</Radio.Button>
                                        <Radio.Button value={'Processing'}>Processing View</Radio.Button>
                                    </Radio.Group>
                                </div>
                                <div style={{position: "relative"}}>
                                    {!isLiveView &&
                                    <div style={{position: "absolute"}}>
                                        <img
                                            src={latestSnapShot}
                                            width={720}
                                            height={480}
                                        />
                                    </div>
                                    }
                                    <div style={{opacity: `${isLiveView ? 1 : 0}`}}>
                                        <Webcam
                                            audio
                                            ref={webcamRef}
                                            screenshotFormat="image/jpeg"
                                            videoConstraints={videoConstraints}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Space>
                    </Space>

                    <Button onClick={handleStartCaptureClick}>
                        {capturing ? 'Stop capture' : 'Capture photo'}
                    </Button>
                    <ChartBar profileId={props.profileId} streamId={stream}/>
                </>
                }
            </Modal>
        </>
    )
}

export default LiveInterview;
