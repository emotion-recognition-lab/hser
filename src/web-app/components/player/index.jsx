import ReactPlayer from "react-player";
import React from "react";
import "./style.less"

export default function Player(props) {
    const [emojis, setEmojis] = React.useState([]);

    const mapping= {
        "Angry" : '😡',
        "Disgust": '🤢',
        "Fear": '😨',
        "Happy":'😃',
        "Neutral": '😐',
        "Sad": '😔',
        "Surprise":'😲'
    };

    const onProgress = React.useCallback(({loaded, loadedSeconds, played, playedSeconds}) => {
    }, [props.url, props.emotions]);
    return (
        <>
            <div className={"emojis"}>
                {props.emotions && React.Children.toArray(props.emotions.map((emotion, index) => {
                    if (props.emotions[index -1] !== emotion) {
                     return (
                         <span style={{marginLeft: `${index * 100 /props.emotions.length}%`}}>
                            {mapping[emotion]}
                        </span>
                     )
                    }
                }))}
            </div>
            {!props.disablePlayer &&
            <ReactPlayer
                url={props.url}
                controls
                onProgress={onProgress}
                progressInterval={1000}
                width='100%'
                height='100%'
            />
            }
        </>
    )
}
