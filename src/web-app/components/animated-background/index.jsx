import './style.less';

const AnimatedBackground = ({children, balloon = 10}) => {
    return (
        <div className="area">
            {children}
            <ul className="circles">
                {React.Children.toArray(Array(balloon).fill(null).map(() => <li/>))}
            </ul>
        </div>
    )
}

export default AnimatedBackground;
