import React from 'react';
import { Modal, Form, Input} from 'antd';
import axios from "axios";

const InterviewCategoryModel = ({
                       visible,
                       onCreate,
                       onCancel,
                       category
                   }) => {
    const [form] = Form.useForm();

    return (
        <Modal
            visible={visible}
            title={category ? "Edit Interview Category" : "Create a new interview category"}
            okText={category ? "Edit " : "Create"}
            cancelText="Cancel"
            onCancel={onCancel}
            destroyOnClose={true}
            onOk={() => {
                form
                    .validateFields()
                    .then(async values => {
                        values.id = category?.id;
                        try {
                            if (values.id) {
                                const resp = await axios.patch('/api/interview/categories', values);
                            } else {
                                const resp = await axios.put('/api/interview/categories', values);
                            }
                            form.resetFields();
                            onCreate();
                        } catch (e) {
                            console.log(e)
                        }

                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form
                form={form}
                layout="vertical"
                name="form_in_modal"
                initialValues={category}
                hideRequiredMark={true}
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[{required: true, message: 'Please input the title!'}]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="details"
                    label="Details"
                    rules={[{required: false, message: 'Please input the family!'}]}
                >
                    <Input.TextArea />
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default InterviewCategoryModel;
