import React, {useEffect, useState} from "react";
import {Button, message, Progress, Result} from 'antd';
import Dragger from "antd/lib/upload/Dragger";
import {CameraTwoTone, SmileOutlined, SyncOutlined} from '@ant-design/icons';
import Modal from "antd/lib/modal/Modal";
import Socket from "../../lib/useSocket";
import {useRouter} from "next/router";

export function UploadVideo (props){
    const [stream, setStream] = useState(props.streamId || Date.now());
    const [isProcessing, SetIsProcessing] = useState(false);
    const [isUploading, setIsUploading] = useState(false);
    const [isProcessed, setIsProcessed] = useState(false);
    const [latestSnapshot, setLatestSnapshot] = useState({});
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [percent, setPercent] = useState(0);
    const router = useRouter();

    const uploadProps = {
        maxCount:1,
        name: 'file',
        fileList:null,
        multiple: false,
        action: '/file/upload',
        progress: null,
        data: {
            streamId: stream,
            category: router.query.category,
            profileId: props.profileId
        },
        onChange(info) {
            setIsModalVisible(true);
            setIsUploading(true);
            const { status, percent } = info.file;
            setPercent(parseFloat(percent).toFixed(2));
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                setIsUploading(false);
                SetIsProcessing(true);

                // props.onUploadFinish(stream);
                Socket.getInstance().emit('UPLOAD_VIDEO_FINISHED', {
                    streamId: stream,
                    category: router.query.category,
                    profile: {id: props.profileId
                    },
                });
            } else if (status === 'error') {
                setIsUploading(false);
                SetIsProcessing(false);
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };

    useEffect(() => {
        const listener = (...args) => {
            SetIsProcessing(false);
            const array = new Array(args);
            const info = array[0][0];
            setLatestSnapshot(info);
            if (info.progress.number  === info.progress.from) {
                setPercent(100);
                setIsProcessed(true);
            } else {
                setPercent(parseFloat(info.progress.number * 100 / info.progress.from).toFixed(2))
            }

        };
        Socket.getInstance().on('SNAPSHOT_PROCESSED', listener);
        return () => {
            Socket.getInstance().off('SNAPSHOT_PROCESSED', listener);
        }
    }, [props.profileId, stream]);


    return (
        <>
            <Dragger {...uploadProps}>
                <p className="ant-upload-drag-icon">
                    <CameraTwoTone />
                </p>
                <p className="ant-upload-text">Offline Interviewer</p>
                <p className="ant-upload-hint">
                    Drag or Upload
                </p>
            </Dragger>
            {isModalVisible &&
            <Modal title="Offline Interview Analyzer"
                   visible={true}
                   destroyOnClose
                   maskClosable={false}
                   width={'90vw'}
                   footer={null}
            >
            <Result
                icon={isProcessed ? <SmileOutlined /> : isProcessing ?  <SyncOutlined spin /> : isUploading ? <Progress type="circle" percent={percent} />: <><img style={{maxHeight: '60vh'}} src={'/' + latestSnapshot.path}/></> }
                title={isProcessed ? "Great, we have done all the operations!" : isProcessing ?  'Preprocessing...' : isUploading ?  "Uploading the file..." : "We are trying to process the interview."}
                extra={isProcessed ?
                    <Button type={'primary'}
                            onClick={() => { setIsModalVisible(false); props.onDone(stream);}}>
                        Go to Result
                    </Button> :
                    isProcessing ? null :
                    isUploading ? null : <Progress percent={percent} />
                }
            />
            </Modal>}
        </>
    )
}
