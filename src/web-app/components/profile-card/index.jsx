import React from 'react'
import {Button, Card, Tooltip} from 'antd';
import { ArrowRightOutlined} from '@ant-design/icons';

const {Meta} = Card;

const ProfileCard = ({profile}) => {
    return (
        <Card
            style={{
                marginBottom: 24,
                width: 300,
            }}
            cover={
                <img
                    style={{
                        height: 199,
                    }}
                    src={profile.path || "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"}
                />
            }
            actions={[
                <Tooltip placement="bottom" color={"cyan"} title={'Number of Interview'}>
                    {profile.scount}
                </Tooltip>,
                <Tooltip placement="bottom" color={"cyan"} title={'Number of Records'}>
                    {profile.records_count | 0}
                </Tooltip>,
                <a href={`/profile/${profile.id}`}><ArrowRightOutlined key="ArrowRightOutlined"/></a>,
            ]}
        >
            <Meta
                title={profile.name}
                description={<>
                    <div>
                    {(new Date(profile.date)).toLocaleString()}
                    </div>
                    <small>
                        {profile.category?.title}
                    </small>
                </>}
            />
        </Card>
    )
}

export default ProfileCard;
