import React, {useEffect, useState} from "react";
import {Alert, Divider, Modal} from "antd";
import "./style.less";
// import VoiceRadarChart from "../charts/voice-radar";
import VoicePieChart from "../charts/voice-pie"
import Axios from "axios";
import {format, formatDistance} from 'date-fns'
import {DropEvent} from "../charts/drop-event";
import TextReport from "../text-report";
import MultiFactorResult from "../multi-factor-result";

export default function Report({voiceData, profileId, stream, showSign}) {

    const [profileData, setProfileData] = useState(null);
    const [snapshotHighlights, setSnapshotHighlights] = useState({});
    const [averageSnapshotEmotions, setAverageSnapshotEmotions] = useState({});
    const [effectiveSnapshotsCount, setEffectiveSnapshotsCount] = useState(0);
    const [records, setRecords] = useState([]);
    const [texts, setTexts] = useState([]);

    useEffect(() => {
        const highlights = {};
        const sums = {};
        let count = 0;

        Axios.get(`/api/profile/${profileId}`)
            .then(res => {
                setProfileData(res.data);
            });

        Axios.get(`/api/profile/${profileId}/records/${stream.stream_id}`)
            .then(res => {
                setRecords(res.data.sort((a, b) => a.date - b.date));
                res.data.forEach(record => {
                    if (!Array.isArray(record.details) || !record.details[0]) {
                        return;
                    }
                    count++;
                    Object.keys(record.details[0]).forEach(emotion => {
                        if (!sums[emotion]) {
                            sums[emotion] = record.details[0][emotion]
                        } else {
                            sums[emotion] = sums[emotion] + record.details[0][emotion]
                        }
                        if (!highlights[emotion] || highlights[emotion].details[0][emotion] < record.details[0][emotion]) {
                            highlights[emotion] = record;
                        }
                    });
                });
                setSnapshotHighlights(highlights);
                setAverageSnapshotEmotions(sums);
                setEffectiveSnapshotsCount(count);
                try {
                    const textsRecords = res.data.filter(r => r.type === 'text').map(txt => ({
                        ...txt,
                        prediction: JSON.parse(txt.prediction),
                        details: typeof txt.details === "string" ? JSON.parse(txt.details) : {}
                    }));
                    setTexts(textsRecords);
                } catch (e) {
                    console.log(e)
                }

            });
    }, []);


    return (
        <div className={'report'} id={'report-container'}>
            <div className={'r-head'}>
                <img src={'/icon.svg'} alt={'Human Emotion Recognition Lab'}/>
                <div className={'r-titles'}>
                    <h2>Human Emotion & Sensitivity Laboratory</h2>
                    <h4>Islamic Azad University - Lahijan Branch (2020-2022)</h4>
                </div>
            </div>
            <Divider/>
            <div className={'table-view'}>
                <div>
                    <div>
                        <b>Interview session:</b> {profileData?.category?.title}
                    </div>
                    <div>
                        <b>Operator:</b> {profileData?.created_by?.name} {profileData?.created_by?.family}
                    </div>
                </div>
                <div>
                    <div>
                        <b>Profile id:</b> {profileData?.id}
                    </div>
                    <div>
                        <b>Interview id:</b> {stream.stream_id.slice(7, 100)}
                    </div>
                </div>
            </div>
            <Divider/>

            <div className={'table-view'}>

                <div>
                    <div>
                        <b>First Name:</b> {profileData?.name}
                    </div>
                    <div>
                        <b>Last Name:</b> {profileData?.family}
                    </div>
                    <div>
                        <b>Gender:</b> {profileData?.gender === 0 && 'Male'}{profileData?.gender === 1 && 'Female'}
                    </div>
                    <div>
                        <b>Age:</b> {profileData?.birthday && formatDistance(new Date(profileData?.birthday), new Date())}
                    </div>
                </div>
                <div>
                    <div>
                        <b>Date:</b> {records[0] && format(new Date(records[0].date), 'yyyy/MM/dd')}
                    </div>
                    <div>
                        <b>Duration:</b> {records[0] && formatDistance(new Date(records[records.length - 1].date), new Date(records[0].date))}
                    </div>
                    <div>
                        <b>Start at:</b> {records[0] && format(new Date(records[0].date), 'HH:mm:ss')}
                    </div>
                    <div>
                        <b>End at:</b> {records[0] && format(new Date(records[records.length - 1].date), 'HH:mm:ss')}
                    </div>
                </div>
            </div>

            <>
                <Divider/>
                <div>
                    <h2>Facial Emotion Results:</h2>
                    {snapshotHighlights && Object.keys(snapshotHighlights).length > 0 ?
                        <>
                            <h3>Samples:</h3>
                            <div className={'face-samples'}>
                                {Object.keys(snapshotHighlights).map((emotion) => (
                                    <div key={emotion}>
                                        <b>{emotion}</b>: <br/> {parseFloat(averageSnapshotEmotions[emotion] / effectiveSnapshotsCount).toFixed(3)}
                                        <img src={'/' + snapshotHighlights[emotion].path} alt=""/>
                                    </div>
                                ))}
                            </div>
                            <h3>Changes Time Series:</h3>
                            <div>
                                <DropEvent profileId={profileId} streamId={stream.stream_id} textData={texts[0]?.meta}/>
                            </div>
                        </>
                        :
                        <Alert
                            message="No Data"
                            description="There is no enough data to display."
                            type="warning"
                        />
                    }

                </div>
            </>
            <Divider/>
            <div>
                <h2>Voice Emotion Results:</h2>
                {voiceData && voiceData.length > 0 ?
                    <>
                        <h3>Changes Time Series:</h3>
                        <div>

                        </div>
                        <h3>Details:</h3>
                        {React.Children.toArray(voiceData.map((voice) => (
                            <VoicePieChart data={voice.prediction} details={voice.details}/>
                            // <VoiceRadarChart data={voice.prediction} details={voice.details}/>
                        )))
                        }
                    </> :
                    <Alert
                        message="No Data"
                        description="There is no enough data to display."
                        type="warning"
                    />
                }
            </div>

            <Divider/>
            <div>
                <h2>Speech Texts Emotion Results:</h2>
                <TextReport records={records} textsItems={texts}/>
            </div>
            <Divider/>
            <div>
                <h2>Multi Factor Emotion Results:</h2>
                <MultiFactorResult records={records}/>
            </div>

            <Divider/>
            <div>
                <h2>Facial Skin Analysis Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            <Divider/>
            <div>
                <h2>Body Language Emotion Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            <Divider/>
            <div>
                <h2>Eeg Signal Emotion Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            <Divider/>
            <div>
                <h2>ECG signal Emotion Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            <Divider/>
            <div>
                <h2>Facial Thermal Image Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            <Divider/>
            <div>
                <h2>Emotions From Walking Results:</h2>
                <Alert
                    message="No Data"
                    description="There is no enough data to display."
                    type="warning"
                />
            </div>
            {showSign &&
                <>
                    <Divider/>
                    <div className={'sign'}>
                        <b>Approval:</b> {profileData?.created_by?.name} {profileData?.created_by?.family}
                        <br/>
                        <b>Date:</b> {format(new Date(), 'yyyy/MM/dd')}
                        <br/>
                        <img src={'/' + profileData?.created_by?.sign} alt={''}/>
                    </div>
                </>
            }
        </div>
    )
}
