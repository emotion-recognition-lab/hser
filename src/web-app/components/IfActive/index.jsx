import React from 'react';
import './style.less';
import {notification} from "antd";

export default function IfActive(props) {
    if (props.active) {
        return props.children;
    }

    const showMessage = () => {
        notification.warning({
            message : 'This feature is not active yet.',
            type: 'warning',
            placement:"bottomRight"
        })
    }

    return (
        <div className={'inactive-div'} onClick={showMessage}>
            {props.children}
            <div className={'overlay'}/>
        </div>
    )
}
