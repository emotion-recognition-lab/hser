import React, {useRef} from 'react'
import {useState} from 'react';
import './../../styles/global.less';
import Axios from "axios";
import {Row, Col, Select, Input, Table, Tag, Button, Avatar, Space, Popconfirm} from 'antd';
import {EditOutlined, UserOutlined, SearchOutlined} from "@ant-design/icons";
import * as PropTypes from "prop-types";
import {format} from "date-fns";

class Highlighter extends React.Component {
    render() {
        return null;
    }
}

Highlighter.propTypes = {
    highlightStyle: PropTypes.shape({padding: PropTypes.number, backgroundColor: PropTypes.string}),
    textToHighlight: PropTypes.any,
    autoEscape: PropTypes.bool,
    searchWords: PropTypes.arrayOf(PropTypes.any)
};
export default function StreamTable() {
    const searchInput = useRef();
    const [streams, setStreams] = useState([]);
    const [interviews, setInterviews] = useState([]);
    const [categories, setCategories] = useState([]);
    const [search, setSearch] = useState('');
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('')
    };

    const handleDelete = stream => {
        Axios.delete(`/api/profile/${stream.profile.id}/stream/${stream.stream_id}`)
            .then(res => {
                const newStreams = streams.filter(s => s.stream_id !== stream.stream_id);
                setStreams(newStreams)
            });
    };

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={searchInput}
                    placeholder={`Search in ${dataIndex}s`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ marginBottom: 8, display: 'block' }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    });


    React.useEffect(() => {
        Axios.get(`/api/streams`)
            .then(res => {
                setStreams(res.data);
                const cats = {};
                const catsArr = [];
                const interviewers = {};
                const interviewersArr = [];
                res.data.forEach(p => {
                    if (p.category && p.category.id && !cats[p.category.id]) {
                        cats[p.category.id] = p.category.title;
                        catsArr.push({
                            id:p.category.id,
                            title:p.category.title
                        })
                    }
                    if (p.created_by && p.created_by.id && !interviewers[p.created_by.user_id]) {
                        interviewers[p.created_by.user_id] = p.created_by.id;
                        interviewersArr.push({
                            id:p.created_by.user_id,
                            name:p.created_by.name,
                            family:p.created_by.family,
                        })
                    }
                });

                setCategories(catsArr);
                setInterviews(interviewersArr);
            })
    }, []);


    const columns = [
        {
            title: 'profile',
            dataIndex: 'profile',
            key: 'profile',
            render: (render => (
                <span>
                    {render.name} {render.family}
                </span>
            )),
            onFilter: (value, record) =>(record.profile.name && record.profile.name.includes(value)) || (record.profile.family && record.profile.family.includes(value)),
            ...getColumnSearchProps('profile'),
        },{
            title: 'National Id',
            dataIndex: 'profile',
            key: 'national_id',
            render: (profile => (
                <span>
                    {profile.national_id}
                </span>
            )),
            onFilter: (value, record) =>(record.profile.national_id && record.profile.national_id.includes(value)),
            ...getColumnSearchProps('national_id'),
        },
        {
            title: 'Interviewer',
            dataIndex: 'created_by',
            key: 'created_by',
            render: created_by => (created_by &&
                <span>
                    <Avatar icon={<UserOutlined />} src={created_by && created_by.photo ? '/' + created_by.photo : null} /> {created_by.name} {created_by.family}
                </span>
            ),
            onFilter: (value, record) => record.created_by && record.created_by.user_id && record.created_by.user_id.includes(value),
            filters: interviews.map(c => ({text: c.name + ' ' + c.family, value:c.id})),
        },
        {
            title: 'Category',
            dataIndex: 'category',
            key: 'category',
            render: category => (category &&
                <span>
                    {category.title}
                </span>
            ),
            filters: categories.map(c => ({text: c.title, value:c.title})),
            onFilter: (value, record) => record.category.title.includes(value),
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
            render: date => (date &&
                <span>
                    {format(new Date(date), 'yyyy/MM/dd HH:mm:ss')}
                </span>
            )
        },

        {
            title: '',
            dataIndex: 'profile',
            key: 'profile',
            render: (profile, stream) => {
                return (
                    <Space size="middle">
                    <a href={`/profile/${profile.id}/interview/${stream.stream_id}`}>
                         Report
                    </a>
                    <Popconfirm
                        title="Are you sure to delete this interview?"
                        onConfirm={() => handleDelete(stream)}
                        okText="Yes"
                        okType={"danger"}
                        placement={"bottomLeft"}
                        cancelText="No"
                    >
                        <Button type={'dashed'}>Delete</Button>
                    </Popconfirm>
                </Space>
            )}
        },
    ];

    return (
        <>
            <Table columns={columns} dataSource={streams} />
        </>
    )
}
