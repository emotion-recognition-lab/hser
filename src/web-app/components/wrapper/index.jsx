import React, {useEffect} from "react";
import {Breadcrumb, Col, Layout, Row, Space} from 'antd';
import Link from "next/link";
import Sidebar from "../sidebar";
import Header from "../header";

const {Footer, Content} = Layout;
import {HomeOutlined} from '@ant-design/icons';
import {useUser} from "../../lib/useUser";
import "../../lib/axios-middleware";
import {useRouter} from 'next/router'
import './../../styles/global.less';

export default function Wrapper(props) {
    const {user} = useUser();
    const router = useRouter()

    // useEffect(() => {
    //     if (!user && isReady) {
    //         router.push('/user/login');
    //     }
    // }, [user, isReady])


    if (!user) {
        return (<></>)
    }

    return (
        <Layout>
            {!props.disableSidebar && <Sidebar/>}
            <Layout style={{minHeight: '100vh'}}>
                {!props.disableSidebar && <Header>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <Link href={'/'}>
                                <HomeOutlined/>
                            </Link>
                        </Breadcrumb.Item>
                        {props.breadcrumb && props.breadcrumb.map((item) => (
                            <Breadcrumb.Item key={item.href}>
                                <Link href={item.href}>
                                    {item.title}
                                </Link>
                            </Breadcrumb.Item>
                        ))}
                    </Breadcrumb>
                </Header>
                }
                <Content style={{padding: 30}}>
                    {props.children}
                </Content>
                <Footer>
                    <Row align={'center'}>
                        <Col>
                            <Space align={'center'} direction={'vertical'}  target={'_blank'}>
                                Powered By <Link href={'https://shosseini.com'}>Sadegh Hosseini</Link>
                            </Space>
                        </Col>
                    </Row>
                </Footer>
            </Layout>
        </Layout>
    )
}
